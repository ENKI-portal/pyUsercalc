{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Simple 2D streamline integration of melting results from stableCalc\n",
    "\n",
    "#### Lynne J. Elkins$^{1}$\n",
    "\n",
    "$^{1}$ University of Nebraska-Lincoln, Lincoln, NE, USA, lelkins@unl.edu"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Summary\n",
    "\n",
    "This Jupyter notebook gathers results from a previous calculation of stable element concentrations in partial melts, using model outputs from the program stableCalc, which is based on pyUserCalc (Elkins and Spiegelman, 2021). This notebook integrates 1-D results over the length of the partial melting column (i.e., over $z$) to determine overall trace element concentrations in partial melts aggregated over a simplified 2-dimensional triangular melting regime. While not truly a two-dimensional flow model (i.e., there is no lateral flow), this model permits estimation of melt mixing that may more accurately reflect magma accumulation beneath divergent tectonic environments. This notebook will be published as an attachment to a manuscript currently in preparation."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Triangular integration of melting\n",
    "\n",
    "This notebook conducts a simple 2D triangular melt integration for a group of stable trace elements, using the basic methods of Asimow et al. (2001) and McKenzie and Bickle (1988):\n",
    "\n",
    "$$\n",
    "    \\overline{c_i^f} = \\frac{\\int_{0}^{z}c_i^f(z) F(z) dz}{\\int_{0}^{z}F(z) dz}\n",
    "$$\n",
    "\n",
    "where $\\overline{c_i^f}$ is the average melt concentration of element $i$ over the entire triangular regime, $z$ is the height of the melting column, $c_i^f(z)$ is the melt concentration at $z$, and $F(z)$ is the degree of melting at $z$. At its simplest, integration of a 1D melting column model over a 2D triangular regime simply sums the mass of the element of interest over that regime and divides it by the total quantity of melt produced to determine an accumulated total concentration."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### The calculator tool\n",
    "\n",
    "The python code cells embedded below implement the models described above, and are designed to be used after running stable trace element calculations separately with the tool stableCalc. A copy of this .ipynb file should thus be saved to the same user directory as the stableCalc model notebook and the stableCalc.py driver code.  A prior stableCalc run for the input scenario of interest should also have already generated a data output or results folder with the necessary files. This tool assumes that the prior data input and model results files have not been renamed or modified, and are organized by the run type and timestamp. After the prior melting run of interest has been calculated, it is only necessary to provide the runname and file path information for the appropriate run, and the relevant results will be retrieved.\n",
    "\n",
    "As with pyUserCalc and stableCalc, once the preliminary calculations have been performed and a fresh kernel started in this notebook, select each embedded code cell by mouse-click, modify the code if necessary, and then simultaneously type the 'Shift' and 'Enter' keys to run the cell, after which selection will automatically advance to the following cell. Note that when modifying and running the model repeatedly, it may be necessary to restart the kernel for each fresh start; Python and Jupyter do not reliably overwrite, purge, or replace prior results.\n",
    "\n",
    "The first cell below imports necessary code libraries to access the Python toolboxes and functions that will be used in the rest of the program. Run this cell."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Select this cell with by mouseclick, and run the code by simultaneously typing the 'Shift' + 'Enter' keys.\n",
    "# If the browser is able to run the Jupyter notebook, a number [1] will appear to the left of the cell.\n",
    "\n",
    "import pandas as pd\n",
    "import numpy as np\n",
    "from scipy import integrate\n",
    "import math\n",
    "import os\n",
    "import csv\n",
    "import shutil\n",
    "import datetime\n",
    "pd.options.mode.chained_assignment = None"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Enter initial input information and view input data\n",
    "\n",
    "Run the first cell below and enter the name of the original run file, such as \"sample\", and the timestamp for the prior run that should be accessed. This directs the program to import the proper stableCalc 1D model solution data from the correct prior run. The cells that follow will verify the transport models used and import the necessary data tables."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Provide the original run name for the scenario to be integrated (this is the filename):\n",
    "runname=input(\"What is the name of your original input data file (minus any file extensions, e.g. 'sample' for 'sample.csv')?\")\n",
    "stamp=input(\"What was the date/timestamp of the model run to be considered here? This can be copied from the output folder name in 'YYYY-MM-DD-HHMMSS' format:\")\n",
    "\n",
    "# runname='sample_stable_model'\n",
    "# stamp = '2024-05-30_091744'"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Create a directory for the outputs from this set of calculations:\n",
    "newstamp = datetime.datetime.now().strftime(\"%Y-%m-%d_%H%M%S\")\n",
    "os.mkdir('outputs/{}/{}/Additional_calculations/{}'.format(runname,stamp,newstamp))\n",
    "os.mkdir('outputs/{}/{}/Additional_calculations/{}/Figures'.format(runname,stamp,newstamp))\n",
    "os.mkdir('outputs/{}/{}/Additional_calculations/{}/Results'.format(runname,stamp,newstamp))\n",
    "\n",
    "runlog = open(\"outputs/{}/{}/Additional_calculations/{}/run_log_{}.txt\".format(runname,stamp,newstamp,newstamp),\"w\")\n",
    "runlog.write('Title: Run log for 2D integration calculations, using initial file {}.csv with previous results from timestamp {}. 2D calculations initiated at {}.\\n'.format(runname,stamp,newstamp))\n",
    "runlog.close()\n",
    "\n",
    "maindir = 'outputs/{}/{}/Results/'.format(runname,stamp)\n",
    "additional = 'outputs/{}/{}/Additional_calculations/'.format(runname,stamp)\n",
    "figures = 'outputs/{}/{}/Additional_calculations/{}/Figures/'.format(runname,stamp,newstamp)\n",
    "results = 'outputs/{}/{}/Additional_calculations/{}/Results/'.format(runname,stamp,newstamp)\n",
    "runlog_file = \"outputs/{}/{}/Additional_calculations/{}/run_log_{}.txt\".format(runname,stamp,newstamp,newstamp)\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Run this cell to specify the correct transport models for this scenario.\n",
    "# Options for each are 'yes' and 'no':\n",
    "\n",
    "# equilibrium = input(\"Calculate 2D integration for results from the equilibrium transport model?\")\n",
    "# disequilibrium = input(\"Calculate 2D integration for results from the disequilibrium (true fractional) transport model?\")\n",
    "# scaled = input(\"Calculate 2D integration for results from a scaled disequilibrium model with a Damkohler number?\")\n",
    "\n",
    "equilibrium = 'yes'\n",
    "disequilibrium = 'yes'\n",
    "scaled = 'yes'\n",
    "\n",
    "if scaled == 'yes':\n",
    "#     Da_number = input(\"What was the Dahmkohler number used for the scaled model?\")\n",
    "    Da_number = '0.1'\n",
    "    Da_number = float(Da_number)\n",
    "else:\n",
    "    Da_number = 'None'\n",
    "    \n",
    "prior_runs = ['',\n",
    "         'Prior results to be used for integration calculations here:',\n",
    "         'Equilibrium transport results: {}'.format(equilibrium),\n",
    "         'Disequilibrium transport results: {}'.format(disequilibrium),\n",
    "         'Scaled disequilibrium transport results with a Damkohler number: {}, with Da = {}'.format(scaled, Da_number),]\n",
    "with open(runlog_file,\"a\") as runlog:\n",
    "    for i in prior_runs:\n",
    "        runlog.write(i+'\\n')\n",
    "runlog.close()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Run the following cells to implement the choices selected above, and to retrieve the data needed from the correct prior model runs."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# The code below will import the data sets needed to run the integration calculations for the prior runs specified\n",
    "# above.\n",
    "\n",
    "if equilibrium == 'yes':\n",
    "    input_file = maindir+'{}_1D_solution_eq.csv'.format(runname)\n",
    "if disequilibrium == 'yes':\n",
    "    input_file2 = maindir+'{}_1D_solution_diseq.csv'.format(runname)\n",
    "if scaled == 'yes':\n",
    "    input_file3 = maindir+'{}_1D_solution_diseq_Da={}.csv'.format(runname,Da_number)\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# This cell generates the necessary dataframes for the input data sets specified above.\n",
    "\n",
    "# First check for comment lines that should be removed to generate the dataframes:\n",
    "comment='Comment'\n",
    "\n",
    "if equilibrium == 'yes':\n",
    "    with open(input_file) as f_obj:\n",
    "        reader = csv.reader(f_obj, delimiter=',')\n",
    "        for line in reader:\n",
    "            if comment in str(line):\n",
    "                df = pd.read_csv(input_file,skiprows=1,dtype=float)\n",
    "            else:\n",
    "                df = pd.read_csv(input_file,dtype=float)\n",
    "                df = df.iloc[: , 1:]\n",
    "            break\n",
    "\n",
    "    runlog = open(runlog_file,\"a\")\n",
    "    runlog.write('\\n')\n",
    "    runlog.write('Import prior equilibrium run results:\\n')\n",
    "    runlog.close()\n",
    "    df.to_csv(runlog_file, index=None, sep=' ', mode='a')\n",
    "    \n",
    "if disequilibrium == 'yes':\n",
    "    with open(input_file2) as f_obj:\n",
    "        reader = csv.reader(f_obj, delimiter=',')\n",
    "        for line in reader:\n",
    "            if comment in str(line):\n",
    "                df2 = pd.read_csv(input_file2,skiprows=1,dtype=float)\n",
    "            else:\n",
    "                df2 = pd.read_csv(input_file2,dtype=float)\n",
    "                df2 = df2.iloc[: , 1:]\n",
    "            break\n",
    "\n",
    "    runlog = open(runlog_file,\"a\")\n",
    "    runlog.write('\\n')\n",
    "    runlog.write('Import prior disequilibrium run results:\\n')\n",
    "    runlog.close()\n",
    "    df2.to_csv(runlog_file, index=None, sep=' ', mode='a')\n",
    "    \n",
    "if scaled == 'yes':\n",
    "    with open(input_file3) as f_obj:\n",
    "        reader = csv.reader(f_obj, delimiter=',')\n",
    "        for line in reader:\n",
    "            if comment in str(line):\n",
    "                df3 = pd.read_csv(input_file3,skiprows=1,dtype=float)\n",
    "            else:\n",
    "                df3 = pd.read_csv(input_file3,dtype=float)\n",
    "                df3 = df3.iloc[: , 1:]\n",
    "            break\n",
    "    \n",
    "    runlog = open(runlog_file,\"a\")\n",
    "    runlog.write('\\n')\n",
    "    runlog.write('Import prior scaled disequilibrium run results:\\n')\n",
    "    runlog.close()\n",
    "    df3.to_csv(runlog_file, index=None, sep=' ', mode='a')\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The cells below will display the input data tables. Verify they are the correct ones before moving on."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "# Equilibrium transport data table:\n",
    "if equilibrium == 'yes':\n",
    "    print(df)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "# Pure disequilibrium transport data table:\n",
    "if disequilibrium == 'yes':\n",
    "    print(df2)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "# Scaled disequilibrium transport data table:\n",
    "if scaled == 'yes':\n",
    "    print(df3)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Set up 2D model integration\n",
    "\n",
    "The cells below use the degrees of melting and the calculated element concentrations to run the integrated mass calculations."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# First, extract the names of the elements to be analyzed:\n",
    "elements = [col for col in df.columns if 'D' in col]\n",
    "names = [col for col in df.columns if 'Cf' in col]\n",
    "newcols = names[:]\n",
    "newcols.insert(0,'F')\n",
    "elements = list(map(lambda i: i[1:], elements))\n",
    "elements"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Next, build copy dataframes for use during calculations:\n",
    "if equilibrium == 'yes':\n",
    "    eqcalcs = df[newcols]\n",
    "if disequilibrium == 'yes':\n",
    "    diseqcalcs = df2[newcols]\n",
    "if scaled == 'yes':\n",
    "    diseqdacalcs = df3[newcols]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# This operation determines elemental masses with depth, and then integrates over the full\n",
    "# depth range of the triangular regime. The results then populate a data table of outcomes.\n",
    "if equilibrium == 'yes':\n",
    "    \n",
    "    # Determine the mass of each element with depth:\n",
    "    for i in elements:\n",
    "        eqcalcs['mass_{}'.format(i)] = df['Cf({})'.format(i)]*df['F']\n",
    "    \n",
    "    # Keep only the columns for integration (F and element masses):\n",
    "    Cfcols = eqcalcs.columns[eqcalcs.columns.str.contains('Cf')]\n",
    "    eqcalcs.drop(Cfcols,axis=1,inplace=True)\n",
    "\n",
    "    # Integrate masses:\n",
    "    intmass = eqcalcs.apply(np.trapz, axis=0, args=(eqcalcs.index,))\n",
    "    masses = pd.DataFrame({'Integrated mass':intmass}).T\n",
    "    df_out = pd.DataFrame({'Concentrations':intmass}).T\n",
    "    df_out = df_out.div(masses['F'].values,axis=0)\n",
    "    df_out.loc[:, ['F']] = masses[['F']].values\n",
    "    df_out.columns = df_out.columns.str.replace('mass_', '')\n",
    "    \n",
    "    runlog = open(runlog_file,\"a\")\n",
    "    runlog.write('\\n')\n",
    "    runlog.write('Integrated results for equilibrium transport:\\n')\n",
    "    runlog.close()\n",
    "    df_out.to_csv(runlog_file, index=None, sep=' ', mode='a')\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "df_out"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "if disequilibrium == 'yes':\n",
    "    \n",
    "    # Determine the mass of each element with depth:\n",
    "    for i in elements:\n",
    "        diseqcalcs['mass_{}'.format(i)] = df2['Cf({})'.format(i)]*df2['F']\n",
    "    \n",
    "    # Keep only the columns for integration (F and element masses):\n",
    "    Cfcols = diseqcalcs.columns[diseqcalcs.columns.str.contains('Cf')]\n",
    "    diseqcalcs.drop(Cfcols,axis=1,inplace=True)\n",
    "\n",
    "    # Integrate masses:\n",
    "    intmass2 = diseqcalcs.apply(np.trapz, axis=0, args=(diseqcalcs.index,))\n",
    "    masses2 = pd.DataFrame({'Integrated mass':intmass2}).T\n",
    "    df_out2 = pd.DataFrame({'Concentrations':intmass2}).T\n",
    "    df_out2 = df_out2.div(masses2['F'].values,axis=0)\n",
    "    df_out2.loc[:, ['F']] = masses2[['F']].values\n",
    "    df_out2.columns = df_out2.columns.str.replace('mass_', '')\n",
    "    \n",
    "    runlog = open(runlog_file,\"a\")\n",
    "    runlog.write('\\n')\n",
    "    runlog.write('Integrated results for disequilibrium transport:\\n')\n",
    "    runlog.close()\n",
    "    df_out2.to_csv(runlog_file, index=None, sep=' ', mode='a')\n",
    "    "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "df_out2"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "if scaled == 'yes':\n",
    "    \n",
    "    # Determine the mass of each element with depth:\n",
    "    for i in elements:\n",
    "        diseqdacalcs['mass_{}'.format(i)] = df3['Cf({})'.format(i)]*df3['F']\n",
    "    \n",
    "    # Keep only the columns for integration (F and element masses):\n",
    "    Cfcols = diseqdacalcs.columns[diseqdacalcs.columns.str.contains('Cf')]\n",
    "    diseqdacalcs.drop(Cfcols,axis=1,inplace=True)\n",
    "\n",
    "    # Integrate masses:\n",
    "    intmass3 = diseqdacalcs.apply(np.trapz, axis=0, args=(diseqdacalcs.index,))\n",
    "    masses3 = pd.DataFrame({'Integrated mass':intmass3}).T\n",
    "    df_out3 = pd.DataFrame({'Concentrations':intmass3}).T\n",
    "    df_out3 = df_out3.div(masses3['F'].values,axis=0)\n",
    "    df_out3.loc[:, ['F']] = masses3[['F']].values\n",
    "    df_out3.columns = df_out3.columns.str.replace('mass_', '')\n",
    "    \n",
    "    runlog = open(runlog_file,\"a\")\n",
    "    runlog.write('\\n')\n",
    "    runlog.write('Integrated results for scaled disequilibrium transport:\\n')\n",
    "    runlog.close()\n",
    "    df_out3.to_csv(runlog_file, index=None, sep=' ', mode='a')\n",
    "    "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "df_out3"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Organize and export the results as one-line data tables with labels and headers:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "if equilibrium == 'yes':\n",
    "    with open(results+\"{}_2D_solution_eq.csv\".format(runname),'a') as comment_eq:\n",
    "        comment_eq.write(\"Comment:,Two-dimensional integrated melt compositions for equilibrium transport melt model results.\\n\")\n",
    "        df_out.to_csv(comment_eq, index=False)\n",
    "        \n",
    "if disequilibrium == 'yes':\n",
    "    with open(results+\"{}_2D_solution_diseq.csv\".format(runname),'a') as comment_diseq:\n",
    "        comment_diseq.write(\"Comment:,Two-dimensional integrated melt compositions for disequilibrium transport melt model results.\\n\")\n",
    "        df_out2.to_csv(comment_diseq, index=False)\n",
    "\n",
    "if scaled == 'yes':\n",
    "    with open(results+\"{}_2D_solution_diseq_Da={}.csv\".format(runname,Da_number),'a') as comment_diseqda:\n",
    "        comment_diseqda.write(\"Comment:,Two-dimensional integrated melt compositions for scaled disequilibrium transport melt model results (Da={}).\\n\".format(Da_number))\n",
    "        df_out3.to_csv(comment_diseqda, index=False)\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.12"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
