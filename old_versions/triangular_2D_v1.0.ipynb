{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Simple 2D streamline integration of melting results from pyUserCalc (DRAFT)\n",
    "\n",
    "#### Lynne J. Elkins$^{1}$, Sarah Lambart$^{2}$\n",
    "\n",
    "$^{1}$ University of Nebraska-Lincoln, Lincoln, NE, USA, lelkins@unl.edu\n",
    "\n",
    "$^{2}$ University of Utah, Salt Lake City, UT, USA"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Summary\n",
    "\n",
    "This Jupyter notebook gathers results from a previous calculation of U-series disequilibria in partial melts, using model outputs from the program pyUserCalc by Elkins and Spiegelman (2021). This notebook, developed as a supplement to a manuscript by Elkins and Lambart *(in prep.)*, integrates the 1-D column results after Elkins and Spiegelman (2021) over the length of the melting column (i.e., over $z$) to determine the summed ($^{230}$Th/$^{238}$U), ($^{226}$Ra/$^{230}$Th), and ($^{231}$Pa/$^{235}$U) ratios in partial melts over a simplified 2-dimensional triangular melting regime. While not truly a two-dimensional flow model (i.e., there is no lateral flow), this model permits estimation of melt mixing that may more accurately reflect magma accumulation beneath divergent tectonic environments. Please cite both of the references above when using this tool."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Developer's Note\n",
    "\n",
    "<span style='color:red'>\n",
    "\n",
    "**NB:** This notebook is a work in progress! For now, the user may wish to manually edit or skip some code cells. Where those changes would be helpful is documented below and is hopefully clear, but this will be streamlined and improved in future versions.\n",
    "</span>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Triangular integration of melting\n",
    "\n",
    "This particular notebook draws from the saved results of prior 1D pyUserCalc melting calculations (Elkins and Spiegelman, 2021) to determine a simple 2D integration, without changing the results from the initial one-dimensional melt transport calculation. Because this method neglects lateral transport of solids or liquids, this is not truly a 2D model solution to the conservation of mass equations after Spiegelman and Elliott (1993) and Spiegelman (2000), but it may nonetheless provide useful insights when evaluating the results of prior 1D model calculations.\n",
    "\n",
    "At its simplest, integration of a 1D melting column model over a 2D triangular regime simply sums the mass of the element of interest over that regime and divides it by the total quantity of melt produced to determine an accumulated total concentration. From Asimow et al. (2001) and after McKenzie and Bickle (1988):\n",
    "\n",
    "$$\n",
    "    \\overline{c_i^f} = \\frac{\\int_{0}^{z}c_i^f(z) F(z) dz}{\\int_{0}^{z}F(z) dz}\n",
    "$$\n",
    "\n",
    "where $\\overline{c_i^f}$ is the average melt concentration of element $i$ over the entire triangular regime, $z$ is the height of the melting column, $c_i^f(z)$ is the melt concentration at $z$, and $F(z)$ is the degree of melting at $z$.\n",
    "\n",
    "Given $U_i^f(z) = ln(c_i^f(z)/c_0(z))$ and $F(z)$ values for each nuclide $i$ from a prior 1D model run after Elkins and Spiegelman (2021), this calculation appears at first glance relatively straightforward. It is, however, made more complex by the effects of the passage of time on short-lived radiogenic nuclides in the U-series decay chains. A triangular regime integration assumes that with lateral distance from the central ridge axis, the total height of melting experienced by a decompressing parcel decreases due to mantle corner flow. Thus, while a parcel in the center of the melting regime may upwell through the full height assumed by the 1D column model, parcels located to the sides cease melting at progressively greater depths with distance, after Langmuir et al. (1992) and references therein. The accumulation of liquid is thus commonly envisioned as occurring by the migration of the fluid along the top of the triangle to the ridge axis, where it is mixed.\n",
    "\n",
    "For U-series, this means that each melting parcel rises and experiences progressive melting over time, generating isotopic disequilibria in both the solid and the melt; and subsequently, the extracted liquid migrates along the top surface of the triangle without melting further, over some unknown period of time. That transport time interval may be controlled by porous or channelized migration rates at the base of or perhaps within the mantle lithosphere, for example. The transport time is poorly constrained, and while it may be rapid, if slow it could have a significant impact on the preservation of U-series disequilibria from the lateral edges of the melting regime. This time interval must therefore be explicitly considered in the model calculations below, and is determined using an explicitly defined lithospheric transport rate for segregated melts with a simple geometric rule for determining the path length along the upper surface of the melt pooling triangle. Once the added transport time at each depth, $t(z)$, is known, the model calculates an age correction using the U-series radioactive decay and ingrowth equations. The \"streamlines\" that are integrated thus have two components: a melting portion that happens until depth $z$, followed by a transport and decay portion that persists to the top of the melting regime.\n",
    "\n",
    "The integration calculation described here thus assumes that all melts can be gathered efficiently to the central ridge axis, where they are pooled and aggregated. This is likely to be inaccurate in two ways: 1) the top of the melting column may be truncated by the presence of cold lithosphere, which would behave like a cold cap that terminates melting and eliminates the top of the triangle, thereby imposing pure lithospheric transport without melting on all melts above a certain depth; and 2) melts produced by streamlines located in the outermost corners of the melting regime may not be able to efficiently reach the center and accumulate, and instead may be \"lost\" in either the migrating asthenosphere or overlying lithosphere without ultimately joining the aggregated liquid.\n",
    "\n",
    "The former scenario, of lithospheric transport for the entire top of the melting regime, is best tackled by a two-stage melting and melt transport calculation prior to using this notebook, in order to produce a modified 1D input data table. A simple solution would be to truncate the melting regime at some depth using the pyUserCalc calculator, and then determine the effects of simple radioactive decay during transport between the base of the lithosphere and the surface, using the decay calculator notebook tool.\n",
    "\n",
    "The latter scenario, where the outer corners cannot efficiently deliver melt to the pooled mixture, can be determined by subtracting the outer corners of the triangle from the aggregate; this is not currently done here, but may be added in a future version."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### The calculator tool\n",
    "\n",
    "The python code cells embedded below implement the integration calculation described above, and are designed to be used after running pyUserCalc melting calculations after Elkins and Spiegelman (2021). A copy of this .ipynb file should thus be saved to the same user directory as the pyUserCalc model notebook and the UserCalc.py driver code. A prior pyUserCalc run for the input scenario of interest should also have already generated a data output or results folder with the necessary files. This tool assumes that the prior data input and model results files have not been renamed or modified. After the prior melting run of interest has been calculated, it is only necessary to provide the original data input runname below, and the relevant results will be retrieved. Note that this notebook can only use prior runs that have saved the entire run results along the column depth (i.e., batch operations that only save the final output cannot be integrated here), and it is currently written to perform and save the results from *one* integration calculation at a time. Combined operations may be added for more efficient calculations in the future.\n",
    "\n",
    "As with pyUserCalc, once the preliminary calculations have been performed and a fresh kernel started in this notebook, select each embedded code cell by mouse-click, modify the code if necessary, and then simultaneously type the 'Shift' and 'Enter' keys to run the cell, after which selection will automatically advance to the following cell. Note that when modifying and running the model repeatedly, it may be necessary to restart the kernel for each fresh start; Python does not reliably overwrite or replace prior results, particularly in Jupyter notebooks.\n",
    "\n",
    "The first cell below imports necessary code libraries to access the Python toolboxes and functions that will be used in the rest of the program. Run this cell."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Select this cell with by mouseclick, and run the code by simultaneously typing the 'Shift' + 'Enter' keys.\n",
    "# If the browser is able to run the Jupyter notebook, a number [1] will appear to the left of the cell.\n",
    "\n",
    "import pandas as pd\n",
    "import numpy as np\n",
    "from scipy import integrate\n",
    "import math"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Enter initial input information and view input data\n",
    "\n",
    "Edit the first cell below with the name and type of the original run file set in quotes, as has been done for the \"sample\" input and equilibrium run below, and then run the cell. This directs the program to import the proper pyUserCalc 1D model solution data from the correct prior run. The second cell will import and display the data to check that they are correct. The second cell provides two options: 1) Import a standard pyUserCalc model run result, and 2) to import a pyUserCalc model run that has had additional decay calculations performed. Note that in this notebook version, only one integration can be performed at a time."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Provide the original run name for the scenario to be integrated:\n",
    "runname='sample'\n",
    "\n",
    "# Provide the transport model run type to import: 'eq' for equilibrium transport, 'diseq' for disequilibrium, \n",
    "# and 'diseq_Da=0.1' for disequilibrium with a Da number of 0.1 (edit as needed):\n",
    "runtype = 'eq'\n",
    "\n",
    "# If the input file is from a previous decay calculation with transport through the lithosphere, define\n",
    "# the decay time that was used (yrs.). This number was saved in the file name. If this will not be considered\n",
    "# for the calculations below, this value will be ignored.\n",
    "totaltime = '1029'"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In the cell below, select whether this integration calculation will be determined for a single one-dimensional pyUserCalc result, or one that has had additional decay calculations performed."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# If using the result of a prior pyUserCalc model run without additional calculations, type 'no'\n",
    "# for the variable below. If using the result of a decay calculation, type 'yes'.\n",
    "decay = 'yes'"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Run the next two cells to implement the choice selected above, and to retrieve the data needed from the correct prior model run."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# This cell implements the choice made above.\n",
    "if decay == 'no':\n",
    "    input_file = '{}/{}_1D_solution_{}.csv'.format(runname,runname,runtype)\n",
    "\n",
    "elif decay == 'yes':\n",
    "    input_file = '{}/{}_1D_solution_{}_transport_time={}.csv'.format(runname,runname,runtype,totaltime)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Retrieve the necessary input data from the prior model run:\n",
    "input_Ds = 'data/{}.csv'.format(runname)\n",
    "df = pd.read_csv(input_file,dtype=float)\n",
    "df_Ds = pd.read_csv(input_Ds,skiprows=1,dtype=float)\n",
    "df['DU'] = df_Ds['DU']\n",
    "df['DTh'] = df_Ds['DTh']\n",
    "df['DRa'] = df_Ds['DRa']\n",
    "df['DPa'] = df_Ds['DPa']\n",
    "df"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Set up 2D model integration\n",
    "\n",
    "Before running the integration calculation, edit the following cell to change the default variables to those desired. Run the cell to save the new values.\n",
    "\n",
    "The physical transport rate (`lith_rate`) is the physical rate of magma migration along the top of the triangular melting regime, in cm/yr. The theta angle (`theta`) is the angle of the triangle's lower corner; by default theta is 45º, but other values can be used if preferred. These two values will be used to determine the transport rate for the extracted magma along each streamline in the 2D integration.\n",
    "\n",
    "The initial nuclide activity values are needed to determine concentrations from activity results. To properly track nuclide concentrations for this integration, these initial activity values provided should match those used in the initial melting calculations."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Physical transport rate in the lithosphere (in cm/yr.):\n",
    "lith_rate = 1.\n",
    "\n",
    "# Theta angle of the lower 2D triangle corner (in degrees):\n",
    "theta = 45.\n",
    "\n",
    "# Final pressure at the top of the triangle (in kbar). This is only used for bookkeeping below:\n",
    "Pfinal = 0.\n",
    "\n",
    "# Initial activity values (should match initial run conditions):\n",
    "alpha0_238U = 1.\n",
    "alpha0_235U = 1.\n",
    "alpha0_230Th = 1.\n",
    "alpha0_226Ra = 1.\n",
    "alpha0_231Pa = 1.\n",
    "\n",
    "# Calculate the effective transport rate of the segregated liquid along the upper triangle surface, in km/yr.:\n",
    "theta_radians = math.radians(theta)\n",
    "tri_rate = math.sin(theta_radians)*lith_rate/1.e5\n",
    "\n",
    "# Define U-series decay constants:\n",
    "lambdas_238 = np.array([1.551e-10,9.158e-6,4.332e-4])\n",
    "lambdas_235 = np.array([9.8486e-10,2.116e-5])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The next cell simply retrieves the partition coefficient ($D_i$) values that were used in the prior melting calculations. These are needed to determine the concentrations below."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Retrieve initial partition coefficients for Cl/C0 calculations:\n",
    "DU = df['DU']\n",
    "DTh = df['DTh']\n",
    "DRa = df['DRa']\n",
    "DPa = df['DPa']\n",
    "\n",
    "DU0 = DU.iloc[0]\n",
    "DTh0 = DTh.iloc[0]\n",
    "DRa0 = DRa.iloc[0]\n",
    "DPa0 = DPa.iloc[0]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For each depth, the cell below determines the transport time for the segregated liquid, calculates nuclide concentrations from $U_i^f = log(c_i^f)$ values, and applies an age correction for transport in the lithosphere at each depth, using the basic U-series decay equations after Zou (2007)'s formulations:\n",
    "\n",
    "$$\n",
    "{}^{238}U = {}^{238}U_0 \\exp(-\\lambda_{238}t)\n",
    "$$\n",
    "\n",
    "$$\n",
    "{}^{230}Th = {}^{230}Th_0 \\exp(-\\lambda_{230}t) + \\frac{\\lambda_{238}}{\\lambda_{230} - \\lambda_{238}} {}^{238}U_0 (\\exp(-\\lambda_{238}t) - \\exp(-\\lambda_{230}t))\n",
    "$$\n",
    "\n",
    "$$\n",
    "{}^{226}Ra = {}^{226}Ra_0 \\exp(-\\lambda_{226}t) + \\frac{\\lambda_{230}}{\\lambda_{226} - \\lambda_{230}} {}^{230}Th_0 (\\exp(-\\lambda_{230}t) - \\exp(-\\lambda_{226}t)) + \\frac{\\lambda_{230} \\lambda_{238}}{\\lambda_{230} - \\lambda_{238}} {}^{238}U_0 \\left[\\frac{\\exp(-\\lambda_{238}t)-\\exp(-\\lambda_{226}t)}{\\lambda_{226} - \\lambda_{238}} - \\frac{\\exp(-\\lambda_{230}t)-\\exp(-\\lambda_{226}t)}{\\lambda_{226} - \\lambda_{2380}}  \\right]\n",
    "$$\n",
    "\n",
    "The cell then determines the mass of each nuclide with depth."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Calculate lithosphere transport time as a function of distance along the top of the triangle and the transport rate:\n",
    "df['transtime'] = (df['depth'])/tri_rate\n",
    "\n",
    "# Determine decay constant scaling values for Zou (2007) decay formulas:\n",
    "lambdas_238_1 = lambdas_238[0]/(lambdas_238[1]-lambdas_238[0])\n",
    "lambdas_238_2 = lambdas_238[1]/(lambdas_238[2]-lambdas_238[1])\n",
    "lambdas_238_3 = (lambdas_238[1]*lambdas_238[0])/(lambdas_238[1]-lambdas_238[0])\n",
    "lambdas_235_1 = lambdas_235[0]/(lambdas_235[1]-lambdas_235[0])\n",
    "\n",
    "# Determine initial nuclide concentrations, c0:\n",
    "c0_238U = alpha0_238U / lambdas_238[0]/DU0\n",
    "c0_230Th = alpha0_230Th / lambdas_238[1]/DTh0\n",
    "c0_226Ra = alpha0_226Ra / lambdas_238[2]/DRa0\n",
    "c0_235U = alpha0_235U / lambdas_235[0]/DU0\n",
    "c0_231Pa = alpha0_231Pa / lambdas_235[1]/DPa0\n",
    "\n",
    "# Concentrations with radioactive decay:\n",
    "df['Cf_238U'] = np.exp(df['Uf_238U'])*c0_238U*(np.exp(-lambdas_238[0]*df['transtime'])) \n",
    "df['Cf_230Th'] = lambdas_238_1 * np.exp(df['Uf_238U'])*c0_238U*(np.exp(-lambdas_238[0]*df['transtime']) - np.exp(-lambdas_238[1]*df['transtime'])) + np.exp(df['Uf_230Th'])*c0_230Th*np.exp(-lambdas_238[1]*df['transtime'])\n",
    "df['Cf_226Ra'] = np.exp(df['Uf_226Ra'])*c0_226Ra*(np.exp(-lambdas_238[2]*df['transtime'])) + lambdas_238_2 * np.exp(df['Uf_230Th'])*c0_230Th*(np.exp(-lambdas_238[1]*df['transtime']) - np.exp(-lambdas_238[2]*df['transtime'])) + lambdas_238_3*np.exp(df['Uf_238U'])*c0_238U*((np.exp(-lambdas_238[0]*df['transtime']) - np.exp(-lambdas_238[2]*df['transtime']))/(lambdas_238[2]-lambdas_238[0]) - (np.exp(-lambdas_238[1]*df['transtime']) - np.exp(-lambdas_238[2]*df['transtime']))/(lambdas_238[2]-lambdas_238[1]))\n",
    "df['Cf_235U'] = np.exp(df['Uf_235U'])*c0_235U*(np.exp(-lambdas_235[0]*df['transtime']))\n",
    "df['Cf_231Pa'] = lambdas_235_1 * np.exp(df['Uf_235U'])*c0_235U*(np.exp(-lambdas_235[0]*df['transtime']) - np.exp(-lambdas_235[1]*df['transtime'])) + np.exp(df['Uf_231Pa'])*c0_231Pa*np.exp(-lambdas_235[1]*df['transtime'])\n",
    "\n",
    "# Determine masses using nuclide concentrations and the total melt produced along each streamline:\n",
    "df['mass_238U'] = df['Cf_238U']*df['F']\n",
    "df['mass_230Th'] = df['Cf_230Th']*df['F']\n",
    "df['mass_226Ra'] = df['Cf_226Ra']*df['F']\n",
    "df['mass_235U'] = df['Cf_235U']*df['F']\n",
    "df['mass_231Pa'] = df['Cf_231Pa']*df['F']\n",
    "\n",
    "# Create sub-array dataframe containing nuclide masses:\n",
    "subdf = df[['F','mass_238U','mass_230Th','mass_226Ra','mass_235U','mass_231Pa']].copy()\n",
    "subdf\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The cell below integrates all nuclide masses over the depth $z$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Integrated mass values:\n",
    "intmass = subdf.apply(np.trapz, axis=0, args=(subdf.index,))\n",
    "\n",
    "# Display integrated values:\n",
    "intmass"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The cell below calculates the accumulated concentrations of each nuclide, and then determines the isotopic activities and activity ratios."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Calculate aggregated nuclide activities:\n",
    "act_238U = intmass[1]*lambdas_238[0]\n",
    "act_230Th = intmass[2]*lambdas_238[1]\n",
    "act_226Ra = intmass[3]*lambdas_238[2]\n",
    "act_235U = intmass[4]*lambdas_235[0]\n",
    "act_231Pa = intmass[5]*lambdas_235[1]\n",
    "\n",
    "# Calculate aggregated activity ratios:\n",
    "act_230Th_238U = act_230Th / act_238U\n",
    "act_226Ra_230Th = act_226Ra / act_230Th\n",
    "act_231Pa_235U = act_231Pa / act_235U\n",
    "\n",
    "# Display results:\n",
    "print('(230Th)/(238U) = {}'.format(act_230Th_238U))\n",
    "print('(226Ra)/(230Th) = {}'.format(act_226Ra_230Th))\n",
    "print('(231Pa)/(235U) = {}'.format(act_231Pa_235U))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The final cell organizes and exports the results as a simple data table. Note that the input conditions selected above will be saved in the output filename below. This means additional, future calculations that consider different input columns (e.g., outputs from different melt transport models, additional lithospheric decay scenarios, etc.) will not overwrite the current results, but rerunning the same conditions will result in files being overwritten."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "df_out = pd.DataFrame([[Pfinal,lith_rate,theta,act_230Th_238U,act_226Ra_230Th,act_231Pa_235U]],columns=['P','Transport rate (cm/yr.)','Theta (º)','(230Th/238U)','(226Ra/230Th)','(231Pa/235U)'])\n",
    "df_out.to_csv(\"{}/{}_2D_solution_{}_{}.csv\".format(runname,runname,runtype,lith_rate))\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.6"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
