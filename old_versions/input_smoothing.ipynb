{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Input File Smoothing Calculator (DRAFT)\n",
    "#### Lynne J. Elkins$^{1}$\n",
    "\n",
    "$^{1}$ University of Nebraska-Lincoln, Lincoln, NE, USA, lelkins@unl.edu"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Summary\n",
    "\n",
    "This notebook aims to address an occasional problem when using the pyUserCalc code for melting model calculations, where an ODE solver fails to find a solution to the melting equations because of sharp discontinuities or change points in the input parameters with depth. The tool below either permits the user to identify the locations of such change points manually, or attempts to locate them using automatic change point finding routines. The notebook then applies a smoothing function for each sharp, discontinuous change, and generates a modified input file that will hopefully be better resolved by the interpolation functions and ODE solvers in the pyUserCalc calculator tool. While this does not always work and sometimes minor manual adjustment of the input data file is still necessary, this notebook provides a fast way to try to fix the problem before attempting a manual edit. Note that this notebook generates a new input file with a different name; it does not overwrite the existing file.\n",
    "\n",
    "To use the tool below, the user should already have identified the problematic input file, and it should be saved in the user's 'data' file directory. Several options are provided for identifying indexed locations that may require smoothing; these are explained in more detail below."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Developer's Note\n",
    "\n",
    "<span style='color:red'>\n",
    "\n",
    "**NB:** This notebook is a work in progress! For now, the user may wish to skip or manually edit some code cells to use them in the way they prefer. The details of what each cell will do is documented below and is hopefully clear, but this process may be streamlined and improved in future versions.\n",
    "</span>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Retrieving and analyzing the problematic input file"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The next cell imports the needed code libraries. Run this cell."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import pandas as pd\n",
    "import numpy as np\n",
    "import scipy\n",
    "from scipy.signal import savgol_filter\n",
    "import matplotlib.pyplot as plt\n",
    "%matplotlib inline\n",
    "import UserCalc"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Next, load and view the input file that needs to be assessed for abrupt changes. Change the run name from 'sample' to the appropriate CSV file name (minus the file extension) to load the desired input file."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Load the problematic input file:\n",
    "runname='sample'\n",
    "input_file = 'data/{}.csv'.format(runname)\n",
    "df_initial = pd.read_csv(input_file,skiprows=1,dtype=float)\n",
    "df_initial"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The next cell displays the input variables with depth, for visual inspection:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Plot unmodified input file for initial observation and confirmation:\n",
    "UserCalc.plot_inputs(df_initial)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "It is often possible to observe and estimate the pressure(s) where any abrupt changes occur simply by inspecting the data table and input figure shown above. One straightforward approach is thus simply to manually identify and list the pressures where the input file needs to be smoothed.\n",
    "\n",
    "In the following cell, you may choose to enter the pressures where the data should be smoothed for this input file. Then run the following two cells to save and confirm those values. Note that the default pressure values shown are simply examples; and any number of values can be entered."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Manually enter the pressure(s) (in kbar) that should be smoothed in the list below.\n",
    "# Syntax: Plist = [value, value, value, ...]\n",
    "\n",
    "Plist = [8,11, 35]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "Pressure = np.array(df_initial['P'])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "length = len(Plist)\n",
    "manual = []\n",
    "\n",
    "for j in range(length):\n",
    "    value = Plist[j]\n",
    "    index = np.where(Pressure == value)\n",
    "    number = int(index[0])\n",
    "    manual.append(number)\n",
    "    \n",
    "manual.sort()\n",
    "print(manual)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Alternatively, it might be unclear where the pyUserCalc ODE solver was finding a discontinuity that it was unable to solve, or there may be several such potential locations. It can sometimes be useful to use one or more methods for automatically locating abrupt changes in the data set by examining the second derivatives of the input variables. The cells below attempt two different methods for identifying possible change points. These techniques sometimes identify additional curves or inflection points that are not in fact discontinuous, so they probably do not need to be smoothed, but one or both methods can also often locate abrupt changes that might cause problems. I recommend running *all* the cells below in order to see what the automatic change point routines can locate; there are a lot of cells because each one attempts one method for a single variable. You will not necessarily need to *use* all of these results: this is simply data gathering."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# First, make each data column an array to be evaluated for discontinuities. This step is\n",
    "# necessary for any of the cells below to work, so do not skip it.\n",
    "\n",
    "F_points=np.array(df_initial['F'])\n",
    "DU_points = np.array(df_initial['DU'])\n",
    "DTh_points = np.array(df_initial['DTh'])\n",
    "DRa_points = np.array(df_initial['DRa'])\n",
    "DPa_points = np.array(df_initial['DPa'])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# First approach for 'F': calculate second derivatives and look for where they are large:\n",
    "window = 101\n",
    "F_der2 = savgol_filter(F_points, window_length=window, polyorder=2, deriv=2)\n",
    "F_max_der2 = np.max(np.abs(F_der2))\n",
    "F_large = np.where(np.abs(F_der2) > F_max_der2/2)[0]\n",
    "F_gaps = np.diff(F_large) > window\n",
    "F_begins = np.insert(F_large[1:][F_gaps], 0, F_large[0])\n",
    "F_ends = np.append(F_large[:-1][F_gaps], F_large[-1])\n",
    "F_changes = ((F_begins+F_ends)/2).astype(np.int)\n",
    "F_changes"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Alternative approach for 'F': peak finding signal routine to locate second derivative peaks:\n",
    "from scipy import signal\n",
    "F_peakind = signal.find_peaks_cwt(F_der2, np.arange(1,1000))\n",
    "F_peakind"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Plot both sets of results for 'F' (first set red, second set blue dots):\n",
    "plt.plot(F_points)\n",
    "plt.plot(F_changes, F_points[F_changes], 'ro')\n",
    "plt.plot(F_peakind, F_points[F_peakind], 'bo')\n",
    "plt.title('Discontinuities in F')\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Repeat for DU:\n",
    "DU_der2 = savgol_filter(DU_points, window_length=window, polyorder=2, deriv=2)\n",
    "DU_max_der2 = np.max(np.abs(DU_der2))\n",
    "DU_large = np.where(np.abs(DU_der2) > DU_max_der2/2)[0]\n",
    "DU_gaps = np.diff(DU_large) > window\n",
    "DU_begins = np.insert(DU_large[1:][DU_gaps], 0, DU_large[0])\n",
    "DU_ends = np.append(DU_large[:-1][DU_gaps], DU_large[-1])\n",
    "DU_changes = ((DU_begins+DU_ends)/2).astype(np.int)\n",
    "DU_changes"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "DU_peakind = signal.find_peaks_cwt(DU_der2, np.arange(1,1000))\n",
    "DU_peakind"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.plot(DU_points)\n",
    "plt.plot(DU_changes, DU_points[DU_changes], 'ro')\n",
    "plt.plot(DU_peakind, DU_points[DU_peakind], 'bo')\n",
    "plt.title('Discontinuities in DU')\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Repeat for DTh:\n",
    "DTh_der2 = savgol_filter(DTh_points, window_length=window, polyorder=2, deriv=2)\n",
    "DTh_max_der2 = np.max(np.abs(DTh_der2))\n",
    "DTh_large = np.where(np.abs(DTh_der2) > DTh_max_der2/2)[0]\n",
    "DTh_gaps = np.diff(DTh_large) > window\n",
    "DTh_begins = np.insert(DTh_large[1:][DTh_gaps], 0, DTh_large[0])\n",
    "DTh_ends = np.append(DTh_large[:-1][DTh_gaps], DTh_large[-1])\n",
    "DTh_changes = ((DTh_begins+DTh_ends)/2).astype(np.int)\n",
    "DTh_changes"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "DTh_peakind = signal.find_peaks_cwt(DTh_der2, np.arange(1,1000))\n",
    "DTh_peakind"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.plot(DTh_points)\n",
    "plt.plot(DTh_changes, DTh_points[DTh_changes], 'ro')\n",
    "plt.plot(DTh_peakind, DTh_points[DTh_peakind], 'bo')\n",
    "plt.title('Discontinuities in DTh')\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Repeat for DRa:\n",
    "DRa_der2 = savgol_filter(DRa_points, window_length=window, polyorder=2, deriv=2)\n",
    "DRa_max_der2 = np.max(np.abs(DRa_der2))\n",
    "DRa_large = np.where(np.abs(DRa_der2) > DRa_max_der2/2)[0]\n",
    "DRa_gaps = np.diff(DRa_large) > window\n",
    "DRa_begins = np.insert(DRa_large[1:][DRa_gaps], 0, DRa_large[0])\n",
    "DRa_ends = np.append(DRa_large[:-1][DRa_gaps], DRa_large[-1])\n",
    "DRa_changes = ((DRa_begins+DRa_ends)/2).astype(np.int)\n",
    "DRa_changes"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "DRa_peakind = signal.find_peaks_cwt(DRa_der2, np.arange(1,1000))\n",
    "DRa_peakind"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.plot(DRa_points)\n",
    "plt.plot(DRa_changes, DRa_points[DRa_changes], 'ro')\n",
    "plt.plot(DRa_peakind, DRa_points[DRa_peakind], 'bo')\n",
    "plt.title('Discontinuities in DRa')\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Repeat for DPa:\n",
    "DPa_der2 = savgol_filter(DPa_points, window_length=window, polyorder=2, deriv=2)\n",
    "DPa_max_der2 = np.max(np.abs(DPa_der2))\n",
    "DPa_large = np.where(np.abs(DPa_der2) > DPa_max_der2/2)[0]\n",
    "DPa_gaps = np.diff(DPa_large) > window\n",
    "DPa_begins = np.insert(DPa_large[1:][DPa_gaps], 0, DPa_large[0])\n",
    "DPa_ends = np.append(DPa_large[:-1][DPa_gaps], DPa_large[-1])\n",
    "DPa_changes = ((DPa_begins+DPa_ends)/2).astype(np.int)\n",
    "DPa_changes"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "DPa_peakind = signal.find_peaks_cwt(DPa_der2, np.arange(1,1000))\n",
    "DPa_peakind"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "plt.plot(DPa_points)\n",
    "plt.plot(DPa_changes, DPa_points[DPa_changes], 'ro')\n",
    "plt.plot(DPa_peakind, DPa_points[DPa_peakind], 'bo')\n",
    "plt.title('Discontinuities in DPa')\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Because the change point location routines used above may have identified some locations that are not actually problematic or discontinuous, two further options are now presented below, to build the list of locations that will actually be worked on.\n",
    "\n",
    "In the first option, the user can simply use the findings above to manually select and enter the index values that should be fixed, as a list.\n",
    "\n",
    "The second option will automatically compile *all* of the identified locations from all of the finding routines above, and will apply a smoothing routines to *everything.* In many (but not all) cases, this second approach is overkill but is also relatively harmless, since the smoothed intervals are generally relatively small and the changes to the input data are mostly minor. This may not be a good choice for all scenarios, though.\n",
    "\n",
    "Both options are provided using the next two cells, which will simply save the results; they will not all be used below and will not overwrite each other. I recommend running both of these cells to save all of the outcomes of the analysis above."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Option 1: Manually enter selected index values with discontinuities to be smoothed, using the results above:\n",
    "selected = [224,304]\n",
    "selected"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Option 2: Save a full list to smooth all points automatically located above:\n",
    "change_all = np.concatenate((F_changes,F_peakind,DU_changes,DU_peakind,DTh_changes,DTh_peakind,DRa_changes,DRa_peakind,DPa_changes,DPa_peakind), axis=None)\n",
    "auto = change_all.tolist()\n",
    "auto.sort()\n",
    "print(auto)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The methods above provided three options for identifying abrupt discontinuities in the input data file: manual identification of those points, manual selection from an automatically generated list, or simply using the full automated list. In the cell below, the user may identify which method is preferred to produce a list of locations to be smoothed using the functions below. (If you want to try more than one of these options to see what happens, this is the cell that should be changed when clearing and restarting the kernel to redo this calculation. Note, however, that rerunning the whole notebook will automatically overwrite the *smoothed* data file.)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Name the method to be used for identifying change point locations in this smoothing operation.\n",
    "# To select the initial, manual list, enter 'manual' below; for values that were picked from the\n",
    "# automatically generated list, enter 'selected'; and for the full automated list, enter 'auto'.\n",
    "method = 'selected'"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Run this cell to implement the choice made above.\n",
    "if method == 'manual':\n",
    "    change_list = manual\n",
    "    \n",
    "elif method == 'selected':\n",
    "    change_list = selected\n",
    "\n",
    "elif method == 'auto':\n",
    "    change_list = auto\n",
    "    \n",
    "change_list"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For the option chosen above, the smoothing function below will replace a series of sequential rows from the input file with modified rows where the variables change more gradually, hopefully making it easier for pyUserCalc to calculate interpolated values and find a numerical solution. However, if any of the index values in the desired list are very close together, only a single smoothing function is needed for that group, making them redundant. The next cell simply finds and removes any particularly close values in your list, treating them like duplicates."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Omit any indices that are too close together for the smoothing routine to distinguish\n",
    "# (that is, because smoothing occurs over a pressure interval, the same operation will cover \n",
    "# several indices if they are very close):\n",
    "\n",
    "usedValues = set()\n",
    "changes = []\n",
    "\n",
    "for v in change_list:\n",
    "    if v not in usedValues:\n",
    "        changes.append(v)\n",
    "\n",
    "        for lv in range(v - 6, v + 7):\n",
    "            usedValues.add(lv)\n",
    "\n",
    "print(changes)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Apply smoothing functions and save a modified pyUserCalc input file"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The following cells use the problematic locations identified above to build a revised, transitional data frame that smooths the input values for F, Kr, DU, DTh, DRa, and DPa immediately above and below each identified row. The default setting applies the smoothing function for an interval of 15 steps (that is, smoothing between the values 7 steps above and 7 steps below the change point). This can be edited by changing the \"N\" value below."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Define the total interval (number of rows/steps) over which the data should be smoothed\n",
    "# on either side of each change point:\n",
    "N = 15\n",
    "\n",
    "P_lambda = 1.\n",
    "HalfN = (N-1)/2"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "# This cell defines a dataframe to be worked on below, without overwriting the original.\n",
    "# It also defines clearer row indexing values so it is easier to replace the relevant rows.\n",
    "\n",
    "df_smooth = df_initial\n",
    "df_smooth.insert(0, 'New_ID', range(0, 0 + len(df_smooth)))\n",
    "df_smooth"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "# This cell uses the identified index locations to define the sharp boundaries, generate\n",
    "# smoothed dataframe snippets, and create a revised dataframe by replacing values in the\n",
    "# working dataframe:\n",
    "\n",
    "for i in range(0,len(changes)):\n",
    "    index = changes[i]\n",
    "    lower = int(index - HalfN)\n",
    "    upper = int(index + HalfN)\n",
    "    P_range = [df_initial.at[lower,'P'],df_initial.at[index,'P'],df_initial.at[upper,'P']]\n",
    "    F_range = [df_initial.at[lower,'F'], df_initial.at[index,'F'], df_initial.at[upper,'F']]\n",
    "    Kr_range = [df_initial.at[lower,'Kr'], df_initial.at[upper,'Kr']]\n",
    "    D_lower = [df_initial.at[lower,'DU'],df_initial.at[lower,'DTh'],df_initial.at[lower,'DRa'],df_initial.at[lower,'DPa']]\n",
    "    D_upper = [df_initial.at[upper,'DU'],df_initial.at[upper,'DTh'],df_initial.at[upper,'DRa'],df_initial.at[upper,'DPa']]\n",
    "    New_ID = [df_initial.at[lower,'New_ID'],df_initial.at[upper,'New_ID']]\n",
    "    df_snip = UserCalc.smoothing(New_ID, P_range, F_range, Kr_range, D_lower, D_upper, N, P_lambda)\n",
    "\n",
    "    df_snip_indexed = df_snip.set_index(['New_ID'])\n",
    "    df_smooth.update(df_snip_indexed, overwrite=True)\n",
    "\n",
    "del df_smooth['New_ID']\n",
    "df_smooth\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Finally, the two cells below display the outcomes of the smoothing functions and export a modified (and renamed) data input file for future pyUserCalc runs."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# View the results in a pyUserCalc input plot:\n",
    "fig = UserCalc.plot_inputs(df_smooth)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Save the results as a revised and renamed CSV file in the data folder:\n",
    "df_smooth.to_csv('data/{}_smoothed.csv'.format(runname))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.6"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
