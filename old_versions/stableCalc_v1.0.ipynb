{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "7498ff88",
   "metadata": {},
   "source": [
    "# stableCalc: A porous flow calculator for stable element concentrations\n",
    "#### Lynne J. Elkins$^{1}$\n",
    "$^1$University of Nebraska-Lincoln, Lincoln, NE, USA, lelkins@unl.edu"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4bf1272b",
   "metadata": {},
   "source": [
    "### Summary\n",
    "This is a tool for calculating stable element concentrations in partial melts and coexisting solids using disequilibrium porous flow, using the pyUserCalc methods of Elkins & Spiegelman (2021) applied to stable trace elements. This notebook determines elemental concentrations with depth in one dimension in a steady-state decompressing system, for variable melting rates and partition coefficients. This version is a preliminary but functional draft that will be edited in future efforts. It can be used in conjunction with the pyUserCalc tool for calculating uranium-series disequilibria in similar systems."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d7f3106e",
   "metadata": {},
   "source": [
    "### The calculator tool\n",
    "\n",
    "The python code cells embedded below implement the model described above. A copy of this .ipynb file should be saved to a user directory, along with the \"stableCalc.py\" driver file, which contains the main calculation functions plus some plotting functions, and one folder named \"data\". The input file should be a comma-delimited text file written in the same format as the \"sample\" file provided, and should be saved to the \"data\" directory. (**Note** that in order to correctly input the data table, the code below checks for a top line of comment text in the input file; but to correctly identify that text, that line *must* start with the word \"Comment\" like in the sample input file provided.) The input files for this script use the same format as pyUserCalc, except partition coefficients for as many elements as desired can be included, using headers of the format \"Di\" (for example, \"DLa\" or \"DTi\"). Initial concentrations will be entered in the code below.\n",
    "\n",
    "This Jupyter notebook can then be run either native on the user's computer using a python software package, such as Jupyter Notebook in the Anaconda package, or from a cloud account in a shared online JupyterLab or JupyterHub environment like the ENKI platform.\n",
    "\n",
    "Once this notebook has been opened, select each embedded code cell by mouse-click and then simultaneously type the 'Shift' and 'Enter' keys to run the cell, after which selection will automatically advance to the following cell. Cells may be edited prior to running to specify the model calculations desired. Note that when modifying and running the model repeatedly, it may be necessary to restart the kernel for each fresh start.\n",
    "\n",
    "The first cell below imports necessary code libraries to access the Python toolboxes and functions that will be used in the rest of the program:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "23a738c4",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Select this cell with by mouseclick, and run the code by simultaneously typing the 'Shift' + 'Enter' keys.\n",
    "# If the browser is able to run the Jupyter notebook, a number [1] will appear to the left of the cell.\n",
    "\n",
    "import pandas as pd\n",
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "%matplotlib inline\n",
    "import os\n",
    "import csv\n",
    "import shutil\n",
    "import warnings\n",
    "warnings.filterwarnings(\"ignore\",category=RuntimeWarning)\n",
    "pd.options.mode.chained_assignment = None\n",
    "\n",
    "# Import stableCalc\n",
    "import stableCalc\n",
    "\n",
    "# Establish the timestamp for starting this model run\n",
    "import datetime\n",
    "stamp = datetime.datetime.now().strftime(\"%Y-%m-%d_%H%M%S\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ed216818",
   "metadata": {},
   "source": [
    "#### Enter initial input information and view input data\n",
    "\n",
    "Edit and run the cells below to enter the name of your input data file and then load and display the input table and figures."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "a35a4cb9",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Fill in the blank to name the run:\n",
    "\n",
    "runname = input(\"What is the name of your input data file (minus any file extensions, e.g. 'sample' for 'sample.csv')?\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "52c42f1f",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Alternate method for run name: manually edit and uncomment below, then run the cell.\n",
    "\n",
    "# runname = 'sample_stable_model'   # stable element sample test file"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "6461f7de",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Set up file directories:\n",
    "maindir = 'outputs/{}'.format(runname)\n",
    "outputs = 'outputs'\n",
    "if not os.path.exists(maindir):\n",
    "    os.mkdir(maindir)\n",
    "\n",
    "stampdir = 'outputs/{}/{}'.format(runname, stamp)\n",
    "if os.path.exists(stampdir):\n",
    "    shutil.rmtree(stampdir)\n",
    "else:\n",
    "    os.mkdir(stampdir)\n",
    "os.mkdir('outputs/{}/{}/Figures'.format(runname,stamp))\n",
    "os.mkdir('outputs/{}/{}/Results'.format(runname,stamp))\n",
    "os.mkdir('outputs/{}/{}/Additional_calculations'.format(runname,stamp))\n",
    "\n",
    "runlog = open(\"outputs/{}/{}/run_log_{}.txt\".format(runname,stamp,stamp),\"w\")\n",
    "runlog.write('Title: Run log for stable element melting using file {}.csv; run initiated at timestamp {}\\n'.format(runname,stamp))\n",
    "runlog.close()\n",
    "\n",
    "runstamp = 'outputs/{}/{}/'.format(runname,stamp)\n",
    "figures = 'outputs/{}/{}/Figures/'.format(runname,stamp)\n",
    "results = 'outputs/{}/{}/Results/'.format(runname,stamp)\n",
    "runlog_file = \"outputs/{}/{}/run_log_{}.txt\".format(runname,stamp,stamp)\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "a3c8966d",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Input pandas dataframe\n",
    "input_file = 'data/{}.csv'.format(runname)\n",
    "\n",
    "comment='Comment'\n",
    "with open(input_file) as f_obj:\n",
    "    reader = csv.reader(f_obj, delimiter=',')\n",
    "    for line in reader:\n",
    "        if comment in str(line):\n",
    "            df = pd.read_csv(input_file,skiprows=1,dtype=float)\n",
    "        else:\n",
    "            df = pd.read_csv(input_file,dtype=float)\n",
    "        break\n",
    "\n",
    "runlog = open(runlog_file,\"a\")\n",
    "runlog.write('\\n')\n",
    "runlog.write('Melting Model: Stable Elements\\n')\n",
    "runlog.write('Input data file:')\n",
    "runlog.close()\n",
    "\n",
    "df.to_csv(runlog_file, index=None, sep=' ', mode='a')\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "abfaafba",
   "metadata": {},
   "outputs": [],
   "source": [
    "df"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "ff8a2049",
   "metadata": {
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "stableCalc.plot_inputs(df);"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f067b95d",
   "metadata": {},
   "source": [
    "#### Modifying inputs for lithospheric transport\n",
    "For scenarios where a modified transport scheme through a lithospheric layer is desired, the cells below will truncate the 1D melting regime at a final melting pressure, $P_{Lithos}$. Run the cells below to define a $P_{Lithos}$ value; set this value to 0.0 if no lithospheric cap is desired. It is also possible to define the pressure of the base of the crust ($P_{crust}$) in case an additional compositional layer is desired.\n",
    "\n",
    "There are two options for how the final melting pressure will then be imposed. The first option deletes all rows from the input table for depths shallower than $P_{Lithos}$, completely truncating the melting calculation.\n",
    "\n",
    "The second (default) option changes the degree of melting increments ($dF$) to a value of 0 for all depths shallower than $P_{Lithos}$, but continues to track transport to the surface. For full disequilibrium transport, this scenario has no effect on melt compositions after reaching $P_{Lithos}$. For equilibrium or partial disequilibrium transport models, the partial melt and the solid will continue to interact and reequilibrate, but no new melt will be produced; this may or may not be a sensible choice depending on the problems being explored (see Elkins & Spiegelman (2021) for further discussion).\n",
    "\n",
    "Both of the described options generate alternative input dataframes, without overwriting the original data input file. Any of the options can then be selected to run using transport models below."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "291491ae",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Run this cell to define the lithospheric boundary pressures.\n",
    "\n",
    "Plithos = input(\"What is the pressure at the base of the lithosphere where melting stops (in kilobars)? For example, for a layer that truncates melting at 5 kbar, answer '5.0'.\")\n",
    "Pcrust = input(\"What is the pressure at the base of the oceanic crust (in kilobars)?\")\n",
    "# Plithos = 5.0\n",
    "# Pcrust = 2.2\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "de028799",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Run this cell to implement the choices made above.\n",
    "\n",
    "Plithos = float(Plithos)\n",
    "Pcrust = float(Pcrust)\n",
    "df_nolith = df[df.P > Plithos]\n",
    "df_lith = df.copy()\n",
    "Pfinal = df_lith.iloc[(df_lith['P']-Plithos).abs().idxmin()]\n",
    "F_max = Pfinal[1].tolist()\n",
    "df_lith.loc[(df_lith['P'] < Plithos),['F']] = F_max\n",
    "\n",
    "lithlist = ['',\n",
    "            '',\n",
    "            'Lithospheric pressure (Pfinal): {} kbar'.format(Plithos),\n",
    "            '',\n",
    "            'Pressure at the base of the crust: {} kbar'.format(Pcrust),\n",
    "            '']\n",
    "with open(runlog_file,\"a\") as runlog:\n",
    "    for i in lithlist:\n",
    "        runlog.write(i+'\\n')\n",
    "runlog.close()\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "acaea72e",
   "metadata": {},
   "source": [
    "The two cells below offer additional options for modifying melt equilibration during lithospheric transport by imposing new, fixed solid-melt partition coefficients in the lithospheric layer (and, optionally, the crust). This is only relevant if reactive lithospheric transport will be calculated below; otherwise, these cell will have no effect on model results. Select the desired option in the first cell below.\n",
    "\n",
    "The option called \"new\" imposes new partition coefficient values at depths shallower than $P_{Lithos}$, and, if desired, again at depths shallower than $P_{crust}$. (Note, however, that very abrupt and large changes in $D_i$ may be difficult for the model to accurately extrapolate and can produce errors, so for a calculation with large partitioning changes at the asthenosphere-lithosphere interface, it can work better to generate a separate input file with a more gradual adjustment in $D_i$ values at layer boundaries.) The alternative option, called \"old,\" sets all lithosphere partition coefficients equal to the partition coefficient values in the input file at $P_{Lithos}$. After editing the partition coefficients to use desired values, run both cells to implement the changes."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "9de43f2f",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Run this cell to specify how partition coefficients will be defined in any lithospheric layers.\n",
    "\n",
    "define_lith_Ds = input(\"Do you wish to define partition coefficients in the lithosphere using 'new' or 'old' values?\")\n",
    "# define_lith_Ds = 'old'"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "b6fc8da8",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Extract list of element names from df headers, save as list:\n",
    "names = [col for col in df.columns if 'D' in col]\n",
    "elements = list(map(lambda i: i[1:], names))\n",
    "\n",
    "# print list:\n",
    "print(elements)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9704a606",
   "metadata": {},
   "source": [
    "Before proceeding, check that the list of elements above is correct and matches the input file.\n",
    "\n",
    "Then, for the full list of elements being analyzed, enter any modified lithospheric and crustal partition coefficients for each element in the cell below. Make sure the order of elements matches the list above. After running the cell below, make sure the result looks correct, and then proceed to the next cells to implement the changes and log the inputs. (Note that if the run scenario will use original partition coefficients or truncate melting at $P_{Lithos}$, these values will be ignored, so they do not need to be edited.)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "89a0b227",
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "# Enter lithospheric concentrations (in ppm) for all elements considered here, in order:\n",
    "LithDs = [1.0405e-3,\n",
    "          1.3795e-3,\n",
    "          0.00001,\n",
    "          0.00001]\n",
    "\n",
    "CrustDs = [1.0405e-3,\n",
    "          1.3795e-3,\n",
    "          0.00001,\n",
    "          0.00001]\n",
    "\n",
    "# Check that the list is correct:\n",
    "for a, b in zip(elements, LithDs):\n",
    "    print(\"D({}) = {} in the lithosphere\".format(a, b))\n",
    "\n",
    "for a, b in zip(elements, CrustDs):\n",
    "    print(\"D({}) = {} in the crust\".format(a, b))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "a6815a42",
   "metadata": {},
   "outputs": [],
   "source": [
    "Dlith = dict(zip(names, LithDs))\n",
    "Dcrust = dict(zip(names, CrustDs))\n",
    "\n",
    "if define_lith_Ds == 'new':\n",
    "    for i in names:\n",
    "        Dlith_i = Dlith.get(\"{}\".format(i))\n",
    "        Dcrust_i = Dcrust.get(\"{}\".format(i))\n",
    "        df_lith.loc[(df_lith['P'] < Plithos),['{}'.format(i)]] = Dlith_i\n",
    "        df_lith.loc[(df_lith['P'] < Pcrust),['{}'.format(i)]] = Dcrust_i\n",
    "\n",
    "elif define_lith_Ds == 'old':\n",
    "    for i in names:\n",
    "        Dlith_i = Pfinal['{}'.format(i)].tolist()\n",
    "        Dcrust_i = Pfinal['{}'.format(i)].tolist()\n",
    "        df_lith.loc[(df_lith['P'] < Plithos),['{}'.format(i)]] = Dlith_i\n",
    "        df_lith.loc[(df_lith['P'] < Pcrust),['{}'.format(i)]] = Dcrust_i\n",
    "\n",
    "df_lith"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "c2b6f6e4",
   "metadata": {},
   "outputs": [],
   "source": [
    "lithmod = ['',\n",
    "           'Define partition coefficients in the lithosphere using \"{}\" values:'.format(define_lith_Ds),\n",
    "           '']\n",
    "\n",
    "with open(runlog_file,\"a\") as runlog:\n",
    "    for i in lithmod:\n",
    "        runlog.write(i+'\\n')\n",
    "    for a, b in zip(elements, LithDs):\n",
    "        runlog.write(\"D({}) = {} in the lithosphere+\\n\".format(a, b))\n",
    "    for a, b in zip(elements, CrustDs):\n",
    "        runlog.write(\"D({}) = {} in the crust+\\n\".format(a, b))\n",
    "runlog.close()\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e576f03e",
   "metadata": {},
   "source": [
    "#### Set up 1D model runs\n",
    "\n",
    "Next, tell the model which data input scenario will be run below. The options available are to calculate transport models using the original data input file (\"original\"), for input values modified to truncate the run at the base of the lithosphere (\"none\"), or for input values modified to consider lithospheric transport (\"lith\")."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "c8855eb6",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Specify the data input scenario that should be run below. Options are 'original', 'none', and 'lith'.\n",
    "\n",
    "data_input_option=input(\"How should the lithosphere be considered in the model calculations? Options are 'original','lith', and 'none'.\")\n",
    "# data_input_option = 'none'\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "097dd8f7",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Run this cell to implement the data input scenario for the rest of the model run.\n",
    "\n",
    "if data_input_option == 'original':\n",
    "    df_input = df\n",
    "    lith_text = \"transport through the lithosphere was run as in the input file from\"\n",
    "\n",
    "elif data_input_option == 'lith':\n",
    "    df_input = df_lith\n",
    "    lith_text = \"transport through the lithosphere was run with modified partition coefficients from\"\n",
    "    \n",
    "elif data_input_option == 'none':\n",
    "    df_input = df_nolith\n",
    "    lith_text = \"transport and melting were truncated at\"\n",
    "    \n",
    "inputnew = ['',\n",
    "            'Lithospheric transport: Magma {} {} kbar.'.format(lith_text,Plithos),\n",
    "            '',\n",
    "            'Updated input run conditions:']\n",
    "with open(runlog_file,\"a\") as runlog:\n",
    "    for i in inputnew:\n",
    "        runlog.write(i+'\\n')\n",
    "runlog.close()\n",
    "\n",
    "df_input.to_csv(runlog_file, index=None, sep=' ', mode='a')\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "66607bc4",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Generate a figure for the input scenario, and save it to the output folder:\n",
    "stableCalc.plot_inputs(df_input)\n",
    "plt.savefig(figures+\"{}_inputs.pdf\".format(runname),transparent=True,dpi=300,bbox_inches='tight')\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "023f7951",
   "metadata": {},
   "source": [
    "Next, define initial values and other input parameters needed to run the model. For the full list of elements being analyzed, enter the initial concentrations $C_i^0$ in ppm for each element in the cell below. Make sure the order of elements matches the list above. After running the cell below, make sure the result looks correct, and then proceed to the next cell to log the inputs for this run scenario."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "b8249764",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Solid and liquid densities in kg/m3:\n",
    "rho_s = 3300.\n",
    "rho_f = 2800.\n",
    "\n",
    "# Enter initial concentrations for all elements, in order (in ppm):\n",
    "C0s = [1.,\n",
    "       2.,\n",
    "       3.,\n",
    "       4.,\n",
    "       5.,\n",
    "       6.,\n",
    "       7.,\n",
    "       8.,\n",
    "       9.]\n",
    "\n",
    "# Storing values and notes for later:\n",
    "eldict = dict(zip(elements, C0s))\n",
    "commentconc = 'Initial element concentrations (C0) are: '\n",
    "for j in range(len(C0s)):\n",
    "    commentconc+=elements[j-1]+'={}ppm; '.format(C0s[j-1])\n",
    "commentconc = commentconc[:-2]\n",
    "\n",
    "# Check the list of initial concentrations:\n",
    "for a, b in zip(elements, C0s):\n",
    "    print(\"Initial {} = {} ppm\".format(a, b))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "5be9f19e",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Save input parameters to run log:\n",
    "params = ['',\n",
    "          'Initial 1D run parameters:',\n",
    "          'Solid density = {} kg/m^3'.format(rho_s),\n",
    "          'Liquid density = {} kg/m^3'.format(rho_f)]\n",
    "with open(runlog_file,\"a\") as runlog:\n",
    "    for i in params:\n",
    "        runlog.write(i+'\\n')\n",
    "    for a, b in zip(elements, C0s):\n",
    "        runlog.write(\"Initial {} = {} ppm\".format(a, b))\n",
    "runlog.close()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d0f86c8e",
   "metadata": {},
   "source": [
    "#### Equilibrium melting\n",
    "Unlike time-dependent calculations for U-series disequilibria, for stable elements there is no difference between equilibrium porous flow and traditional batch melting (after Shaw, 1975). The cells below determine batch melting results for the inputs above and save the results to an output data table; these can be directly compared with results for equilibrium porous flow melting. If equilibrium melting is not required, skip to the 'Disequilibrium melting' section below."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "a53c914d",
   "metadata": {
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "# Calculate batch melting outcomes with depth for all elements in the input file:\n",
    "df_out_eq = df_input\n",
    "C0 = dict(zip(names, C0s))\n",
    "for i in names:\n",
    "    element = i[1:]\n",
    "    C0i = C0.get(\"{}\".format(i))\n",
    "    Di = df.loc[:,'{}'.format(i)]\n",
    "    df_out_eq['Cf({})'.format(element)] = (C0i) / (df_out_eq.F - df_out_eq.F * Di + Di)\n",
    "    df_out_eq['Cs({})'.format(element)] = (C0i * Di) / (df_out_eq.F - df_out_eq.F * Di + Di)\n",
    "\n",
    "df_out_eq"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a6cdbd71",
   "metadata": {},
   "source": [
    "The cells below will generate a brief table showing output results at the top of the melting column. Run the cell to verify a logical outcome:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "2fb4b7f8",
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "# Equilibrium model result:\n",
    "last_eq = df_out_eq.tail(n=1)\n",
    "print(last_eq)   \n",
    "    \n",
    "runlog = open(runlog_file,\"a\")\n",
    "runlog.write('\\n')\n",
    "runlog.write('Equilibrium melting results: \\n')\n",
    "runlog.close()\n",
    "df_out_eq.to_csv(runlog_file, index=None, sep=' ', mode='a')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5887c71f",
   "metadata": {},
   "source": [
    "Run the next set of cells to view the model output results as a depth figure, and to save it to the output folder."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "99d44576",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Equilibrium model results figure:\n",
    "fig = stableCalc.plot_1Dcolumn(df_out_eq)\n",
    "plt.savefig(figures+\"{}_1D_depths_eq.pdf\".format(runname),transparent=True,dpi=300,bbox_inches='tight')\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3b7e7eb1",
   "metadata": {},
   "source": [
    "Finally, run the cell below to save the results as .csv file in the output folder:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "46fa839b",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Save equilibrium results:\n",
    "with open(results+\"{}_1D_solution_eq.csv\".format(runname), 'a') as comment_eq:\n",
    "    comment_eq.write(\"Comment:, 1D equilibrium transport model results in ppm. Magma {} at {} kbar. \".format(lith_text,Plithos)+commentconc+'.\\n')\n",
    "    df_out_eq.to_csv(comment_eq, index=False)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0b5ad529",
   "metadata": {},
   "source": [
    "#### Disequilibrium melting\n",
    "While equilibrium porous flow (Da = $\\infty$) is equivalent to batch melting, and true disequilibrium porous flow (Da = 0) is the same as true fractional melting for stable elements, solving for elemental concentrations with intermediate chemical reaction rates is more complex. For melting rates and bulk partition coefficients that vary non-linearly during melting, every melt transport scenario other than pure equilibrium is likely to require a numerical solution to differential equations. The cells below use the methods of Elkins & Spiegelman (2021) to accomplish this.\n",
    "\n",
    "Run the cell below to specify whether both true fractional melting and a scaled disequilibrium melting scenario will be run. For the scaled scenario, define a Damköhler number for the melting scenario to be tested. (A Damköhler number is the ratio between a chemical reaction rate (like the melting rate) and a physical transport rate, such as a solid upwelling rate.)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "6fe750f7",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Run this cell to specify the correct transport models for this scenario.\n",
    "# Options for each are 'yes' and 'no'.\n",
    "\n",
    "disequilibrium = input(\"Run the pure disequilibrium (true fractional) transport model?\")\n",
    "scaled = input(\"Run a scaled disequilibrium model with a Damkohler number?\")\n",
    "# disequilibrium = 'yes'\n",
    "# scaled = 'yes'\n",
    "\n",
    "if scaled == 'yes':\n",
    "    Da_number = input(\"What is the Dahmkohler number for the scaled model?\")\n",
    "#     Da_number = '0.1'\n",
    "    Da_number = float(Da_number)\n",
    "else:\n",
    "    Da_number = 'None'\n",
    "\n",
    "run1D = ['',\n",
    "         'Run conditions for 1D calculations:',\n",
    "         'Disequilibrium transport: {}'.format(disequilibrium),\n",
    "         'Scaled disequilibrium transport with a Damkohler number: {}, with Da = {}'.format(scaled, Da_number)]\n",
    "with open(runlog_file,\"a\") as runlog:\n",
    "    for i in run1D:\n",
    "        runlog.write(i+'\\n')\n",
    "runlog.close()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "034bc172",
   "metadata": {},
   "source": [
    "The next cell initializes and runs 1D calculations for the specified transport models and input data.\n",
    "\n",
    "Note that for input data files that generate very rapid concentration changes, the disequilibrium model may occasionally fail because the ODE solver cannot solve the problem, particularly for true fractional transport. To try to prevent this, we recommend first testing $Da=10^{-10}$ instead of $Da=0$ by default. This will often avoid the error and should produce a comparable result, as explored in Elkins and Spiegelman (2021). If errors persist, revising the input data to create more gradual changes in $F(P)$ or $D_i(P)$ may be necessary."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "b1acfe13",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Run these cells to calculate results for the transport models specified above.\n",
    "if disequilibrium == 'yes':\n",
    "    us_diseq = stableCalc.stableCalc(df_input,Da=1.0e-10)\n",
    "    df_out_diseq = us_diseq.solve_all_1D(C0s,elements)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "4a595054",
   "metadata": {},
   "outputs": [],
   "source": [
    "if scaled == 'yes':\n",
    "    us_diseqda = stableCalc.stableCalc(df_input,Da=Da_number)\n",
    "    df_out_diseqda = us_diseqda.solve_all_1D(C0s,elements)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "12c500c7",
   "metadata": {},
   "source": [
    "The cells below will generate brief tables showing output results at the top of the melting column. Run these cells to verify a logical outcome:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "2caa356f",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Pure disequilibrium model result:\n",
    "if disequilibrium == 'yes':\n",
    "    last_diseq = df_out_diseq.tail(n=1)\n",
    "    print(last_diseq)\n",
    "    \n",
    "    runlog = open(runlog_file,\"a\")\n",
    "    runlog.write('\\n')\n",
    "    runlog.write('Disequilibrium melting results: \\n')\n",
    "    runlog.close()\n",
    "    df_out_diseq.to_csv(runlog_file, index=None, sep=' ', mode='a')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "be81eed9",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Scaled disequilibrium model result:\n",
    "if scaled == 'yes':\n",
    "    last_diseqda = df_out_diseqda.tail(n=1)\n",
    "    print(last_diseqda)\n",
    "    \n",
    "    runlog = open(runlog_file,\"a\")\n",
    "    runlog.write('\\n')\n",
    "    runlog.write('Scaled disequilibrium melting results: \\n')\n",
    "    runlog.close()\n",
    "    df_out_diseqda.to_csv(runlog_file, index=None, sep=' ', mode='a')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "165b3488",
   "metadata": {},
   "source": [
    "Run the next set of cells to view the model output results as depth figures, and to save them to the output folder."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "6b6f96b3",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Pure disequilibrium model results figure:\n",
    "if disequilibrium == 'yes':\n",
    "    fig = stableCalc.plot_1Dcolumn(df_out_diseq)\n",
    "    plt.savefig(figures+\"{}_1D_depths_diseq.pdf\".format(runname),transparent=True,dpi=300,bbox_inches='tight')\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "b7573fdd",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Scaled disequilibrium model results figure:\n",
    "if scaled == 'yes':\n",
    "    fig = stableCalc.plot_1Dcolumn(df_out_diseqda)\n",
    "    plt.savefig(figures+\"{}_1D_depths_diseq_Da={}.pdf\".format(runname,us_diseqda.Da),transparent=True,dpi=300,bbox_inches='tight')\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "276ae9dd",
   "metadata": {},
   "source": [
    "Finally, run the appropriate cells below to save the results as .csv files in the output folder:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "017f3f24",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Save pure disequilibrium results:\n",
    "if disequilibrium == 'yes':\n",
    "    with open (results+\"{}_1D_solution_diseq.csv\".format(runname),'a') as comment_diseq:\n",
    "        comment_diseq.write(\"Comment:, 1D disequilibrium transport (Da={}) model results in ppm. Magma {} at {} kbar. \".format(us_diseq.Da,lith_text,Plithos)+commentconc+'.\\n')\n",
    "        df_out_diseq.to_csv(comment_diseq, index=False)\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "8821d4d5",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Save scaled disequilibrium results:\n",
    "if scaled == 'yes':\n",
    "    with open (results+\"{}_1D_solution_diseq_Da={}.csv\".format(runname,us_diseqda.Da),'a') as comment_diseqda:\n",
    "        comment_diseqda.write(\"Comment:, 1D disequilibrium transport (Da={}) model results in ppm. Magma {} at {} kbar. \".format(us_diseqda.Da,lith_text,Plithos)+commentconc+'.\\n')\n",
    "        df_out_diseqda.to_csv(comment_diseqda, index=False)\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "ba97011d",
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.12"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
