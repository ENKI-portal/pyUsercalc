{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Simple 2D streamline integration of melting results from pyUserCalc: stable elements\n",
    "\n",
    "#### Lynne J. Elkins$^{1}$\n",
    "\n",
    "$^{1}$ University of Nebraska-Lincoln, Lincoln, NE, USA, lelkins@unl.edu"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Summary\n",
    "\n",
    "This Jupyter notebook calculates 2-dimensional triangular integrated stable element concentrations in partial melts."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Triangular integration of melting\n",
    "\n",
    "This notebook conducts a simple 2D triangular melt integration for a group of stable trace elements, using the basic methods of Asimow et al. (2001) and McKenzie and Bickle (1988):\n",
    "\n",
    "$$\n",
    "    \\overline{c_i^f} = \\frac{\\int_{0}^{z}c_i^f(z) F(z) dz}{\\int_{0}^{z}F(z) dz}\n",
    "$$\n",
    "\n",
    "where $\\overline{c_i^f}$ is the average melt concentration of element $i$ over the entire triangular regime, $z$ is the height of the melting column, $c_i^f(z)$ is the melt concentration at $z$, and $F(z)$ is the degree of melting at $z$. At its simplest, integration of a 1D melting column model over a 2D triangular regime simply sums the mass of the element of interest over that regime and divides it by the total quantity of melt produced to determine an accumulated total concentration."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### The calculator tool\n",
    "\n",
    "The python code cells embedded below implement the models described above, and are designed to be used after running stable trace element calculations separately. A copy of this .ipynb file should thus be saved to a user directory along with a \"data\" folder that contains an additional \"2D_inputs\" folder with any input files. Those files should have an initial column called \"F\" with the degrees of melting at each pressure step, and a series of columns for each desired element with the *mass* of each element at each pressure step (where the mass is equal to the concentration multiplied by the degree of melting, $F$). These column headers should be labeled with the element name.\n",
    "\n",
    "As with pyUserCalc, once the preliminary calculations have been performed and a fresh kernel started in this notebook, select each embedded code cell by mouse-click, modify the code if necessary, and then simultaneously type the 'Shift' and 'Enter' keys to run the cell, after which selection will automatically advance to the following cell. Note that when modifying and running the model repeatedly, it may be necessary to restart the kernel for each fresh start.\n",
    "\n",
    "The first cell below imports necessary code libraries to access the Python toolboxes and functions that will be used in the rest of the program:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Select this cell with by mouseclick, and run the code by simultaneously typing the 'Shift' + 'Enter' keys.\n",
    "# If the browser is able to run the Jupyter notebook, a number [1] will appear to the left of the cell.\n",
    "\n",
    "import pandas as pd\n",
    "import numpy as np\n",
    "from scipy import integrate\n",
    "import math"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Enter initial input information and view input data\n",
    "\n",
    "Edit the first cell below with the name and type of the original run file set in quotes, as has been done for the \"sample\" input and equilibrium run below, and then run the cell. This directs the program to import the proper pyUserCalc 1D model solution data from the correct prior run. The second cell will import and display the data to check that they are correct."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Provide the original run name for the scenario to be integrated (this is the filename):\n",
    "runname='sample_2D'"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "# Import the appropriate data file:\n",
    "input_file = 'data/2D_inputs/{}.csv'.format(runname)\n",
    "df = pd.read_csv(input_file,dtype=float)\n",
    "df"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Set up 2D model and integrate"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The cells below use the degrees of melting and the masses from the input file to identify the elements of interest and run the calculations. The first cell simply integrates each column to get the total integrated sum:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "# Integrated mass values:\n",
    "intmass = df.apply(np.trapz, axis=0, args=(df.index,))\n",
    "intmass"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The cell below calculates the averaged concentrations, using the mass of each elements divided by the total amount of liquid."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "masses = pd.DataFrame({'Integrated mass':intmass}).T\n",
    "df_out = pd.DataFrame({'Concentrations':intmass}).T\n",
    "df_out = df_out.div(masses['F'].values,axis=0)\n",
    "df_out.drop('depth', inplace=True, axis=1)\n",
    "df_out.loc[:, ['F']] = masses[['F']].values\n",
    "df_out.rename(columns={\"F\": \"Total liquid\"}, inplace=True)\n",
    "\n",
    "df_out"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Organize and export the results as a one-line data table with headers:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "with open(\"2D_results/{}.csv\".format(runname),'a') as comment:\n",
    "    comment.write(\"Comment:,Two-dimensional integrated melt compositions for stable elements.\\n\")\n",
    "    df_out.to_csv(comment, index=False)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.12"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
