# pyUsercalc

A set of Jupyter notebooks that determine U-series disequilibria and trace element concentrations during decompression partial
melting by reactive porous flow. Replicates the functionality of Marc Spiegelman&#39;s UserCalc calculator in Python, and adds
disequilibrium transport and scaled reactive transport calculators. Incorporates methods and tools from Elkins & Spiegelman (2021), 
Elkins et al. (2019, 2023), Elkins & Lambart (2024), and current manuscripts in preparation (see reference list below).

## Contents

* `pyUserCalc_manuscript.ipynb`: Primary Elkins and Spiegelman (2021) manuscript notebook describing derivations, model implementation, and multiple examples for running the U-series calculators.
* `UserCalc.py`:  Python file containing the UserCalc driver and model classes as well as some convenient visualization methods, and which is imported in the model notebooks, mainly for U-series calculations. Cite Elkins and Spiegelman (2021) for general use.
* `stableCalc.py`:  Python file containing the stableCalc stable element driver and model classes with plotting tools, and which is imported in the stable element calculation notebooks. Cite Elkins and Lambart (2024).
*  `data`: Directory containing input .csv files. These files are necessary for the primary notebook to run, and can also be used as templates for additional model use. Some files (like the sample stable element input) are purely for testing and for use as data templates. Includes example .csv files for mineral modes and degree of melting with depth, and for mineral/melt partition coefficients, for use with the input file generator.
*  `outputs`: Empty directory where outputs will be saved when code is run.

#### Additional materials

* `pyUserCalc-v3.3.ipynb`: Simplified version of the primary Elkins and Spiegelman (2021) pyUserCalc notebook, intended for production work with the model. Includes basic equiline plots and run logs. Cite Elkins and Spiegelman (2021) for general use.
* `stableCalc_v1.1.ipynb`: Stable element calculation tool that works with all transport models, for generating trace element concentrations in the melt and solid; can be run in parallel with pyUserCalc for U-series using matching input parameters. Cite Elkins and Lambart (2024) for general use.
* `bulk_Ds_input_generator_v1.0.ipynb`: Notebook that generates input files for the pyUserCalc and stableCalc melt modeling tools, from mineral modes and degrees of melting with depth and from mineral/melt partition coefficients.
* `twolayermodel_v2.0.ipynb`: Notebook containing calculator tool to generate two-layer input files. Now can generate both U-series and stable element input files. Original version was published in Elkins and Spiegelman (2021).
* `input_smoothing_v2.0.ipynb`: Notebook containing calculator tool to attempt to smooth out discontinuous or abrupt changes in pyUserCalc input files. Now can modify any kind of input file, whether for U-series activities or stable element concentrations.
* `Useries_decay_v1.1.ipynb`: Notebook that applies radioactive decay equations to calculate simple decay effects on prior pyUserCalc melting model outcomes. Included in Elkins and Lambart (2024).
* `triangular_2D_v1.1.ipynb`: Notebook containing calculator tool to integrate pyUserCalc melting model results over a simple 2D triangular regime. Included in Elkins and Lambart (2024).
* `triangular_2D_stable_v1.0.ipynb`: Notebook containing calculator tool to integrate stableCalc stable element concentration results over a simple 2D triangular regime.
* `pyDynamic_v1.1.ipynb`: A notebook containing a calculator tool to calculate dynamic melting outcomes, for direct comparison with UserCalc porous flow results and with older dynamic melting methods. Includes plot and table outputs. This code corrects errors in the Matlab script used in Elkins et al. (2019) and reproduces the functionality of Richardson and McKenzie (1994) and Zou and Zindler (2000) in python. The latest version adds radioactive decay during transport of segregated melts using a user-defined transport rate. Published in Elkins et al. (2023).
* `pyDynamicConstant_v1.0.ipynb`: A notebook containing a simple dynamic melting calculator with constant melting rate and partition coefficients, for comparison with older models. This script is useful to test the numerical solutions for dynamic melting in `pyDynamic_v1.0.ipynb` against the simpler formulas used in some of the literature.
* `latex`: Directory containing LaTeX version and formatted PDF of the Elkins and Spiegelman (2021) manuscript for publication.

## Installation/Running

#### ENKI Cloud Server:
Instructions:

* Register for a free [GitLab account](https://gitlab.com/ENKI-portal), if you do not already have one.
* Use your GitLab account to log in to the [ENKI cloud server](https://server.enki-portal.org/hub/login). Note that logging in for the first time can take a few minutes while your server is being built.
* Once you are logged in, click "Close this Screen" button at the bottom of the welcome screen.
* In the Launcher tab, use the "Terminal" button to launch a terminal window.
* At the terminal prompt, type the following text to clone the pyUserCalc code repository to your own server, and then type Return:
`git clone https://gitlab.com/ENKI-portal/pyUsercalc.git`
* Now navigate to the `pyUserCalc` directory in the left sidebar panel and double-click to open it. To access the primary manuscript notebook, open the file `pyUserCalc_manuscript.ipynb`, again by double clicking. To access the production notebook version, instead open the file `pyUserCalc-v3.3.ipynb`. Stable elements may be calculated using the same transport models with the `stableCalc_v1.0.ipynb` notebook.



#### Binder


You can also interact directly with the Elkins and Spiegelman (2021) manuscript notebook through a binder container. To do this, click the button below:

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/ENKI-portal%2FpyUsercalc/master?filepath=pyUserCalc_manuscript.ipynb)

and a container will be built that can run the manuscript notebook through your own browser. This may take a few moments to start. We note that at times, the external binder tool may become unavailable, in which case this notebook link may not fully load. We recommend trying again at another time if this occurs.


#### Local Install

This repository should run with a standard Jupyter installation e.g. through Anaconda (python3).  To do so, clone the repository and open it in JupyterLab or Jupyter Notebook. 


#### Issues, Bugs, and Requests

Have a problem or encounter a bug in the code? Or do you have a developer request for future versions and extensions? Use the "Issues" option on the left sidebar in GitLab and start a "New issue." Alternately, you can [send us an email](mailto:contact-project+enki-portal-pyusercalc-6833634-issue-@incoming.gitlab.com).

Please note that any code or notebook file labeled "DRAFT" or described as a "preliminary draft" above has been made available for public use without peer review, which means 1) it functions but is a work in progress, 2) it will be more streamlined and cleaned up in the future, and 3) the authors should still be fully credited for their work if the tool is used for research purposes. All files in this directory are protected by an open-source copyright license.



### References

Elkins, L.J. and Lambart, S., 2024, Uranium-series disequilibria in MORB, revisited: A systematic numerical approach to partial melting of a heterogeneous mantle, Volcanica v. 7(2), 685-715. doi: 10.30909.vol.07.02.685715.

Elkins, L.J., Bourdon, B., and S. Lambart, 2023, Corrigendum to “Testing pyroxenite versus peridotite sources for marine basalts using U-series isotopes” [Lithos v. 332-333, 226-244] Lithos 452-453, 107217, doi: 10.1016/j.lithos.2023.107217.

Elkins, L.J. and Spiegelman, M., 2021, pyUserCalc: A revised Jupyter notebook calculator for uranium-series disequilibria in basalts. Earth and Space Science v. 8, e2020EA001619, https://doi.org/10.1029/2020EA001619. (Now also available as a Notebooks Now! AGU publication at: https://agu.curve.space/articles/NN0002.)

Elkins, L.J., Bourdon, B., and S. Lambart, 2019, Invited review: Testing pyroxenite versus peridottie sources for marine basalts using U-series isotopes. Lithos v. 332-333, 226-244, doi: 10.1016/j.lithos.2019.02.011.
