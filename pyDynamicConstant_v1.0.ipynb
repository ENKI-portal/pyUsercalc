{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "6f166f5e",
   "metadata": {},
   "source": [
    "# pyDynamic Constant: A simple dynamic melting tool\n",
    "#### Lynne J. Elkins$^{1}$\n",
    "$^1$University of Nebraska-Lincoln, Lincoln, NE, USA, lelkins@unl.edu"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3cb4b84d",
   "metadata": {},
   "source": [
    "### Summary\n",
    "This tool calculates the analytical solution to dynamic melting, by determining partial melt compositions for a constant melting rate and constant bulk rock partition coefficients. While simplistic, for some applications this is the best model to use, for two main reasons: 1) the impacts of changes within the melting regime are small for this style of melting, because only the deep, trace element-rich part of the system significantly influences the accumulated melts; and 2) a great deal of the literature uses this approach, and sometimes it is necessary or helpful to test, compare, and reproduce those methods.\n",
    "\n",
    "For input scenarios that do not have constant melting rate or partition coefficients, the model uses the initial partition coefficients as constant values throughout the melting run, and determines a simple best-linear fit to the degree of melting to determine an approximate constant melting rate."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ef6ee3a9",
   "metadata": {},
   "source": [
    "### The calculator tool\n",
    "\n",
    "The python code cells embedded below implement the model described above . A copy of this .ipynb file should be saved to a user directory, along with the \"UserCalc.py\" driver file, which contains some useful plotting functions, and one folder named \"data\". The input file should be a comma-delimited text file written in the same format as the \"simple_sample\" file provided, and should be saved to the \"data\" directory. (**Note** that in order to correctly input the data table, the code below checks for a top line of comment text in the input file; but to correctly identify that text, that line *must* start with the word \"Comment\" like in the sample input file provided.) This Jupyter notebook can then be run either locally on the user's computer using a python software package, such as Jupyter Notebook in the Anaconda package, or from a cloud account in a shared online JupyterLab or JupyterHub environment like the ENKI platform.\n",
    "\n",
    "Once this notebook has been opened, select each embedded code cell by mouse-click and then simultaneously type the 'Shift' and 'Enter' keys to run the cell, after which selection will automatically advance to the following cell. Cells may be edited prior to running to specify the model calculations desired. Note that when modifying and running the model repeatedly, it may be necessary to restart the kernel for each fresh start.\n",
    "\n",
    "The first cell below imports necessary code libraries to access the Python toolboxes and functions that will be used in the rest of the program:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "7d2fac07",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Select this cell with by mouseclick, and run the code by simultaneously typing the 'Shift' + 'Enter' keys.\n",
    "# If the browser is able to run the Jupyter notebook, a number [1] will appear to the left of the cell.\n",
    "\n",
    "import pandas as pd\n",
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "%matplotlib inline\n",
    "from scipy.integrate import solve_ivp\n",
    "from scipy.interpolate import interp1d, pchip\n",
    "import math\n",
    "import os\n",
    "import csv\n",
    "import shutil\n",
    "import warnings\n",
    "warnings.filterwarnings(\"ignore\",category=RuntimeWarning)\n",
    "import traceback\n",
    "\n",
    "# Import UserCalc plotting functions\n",
    "import UserCalc\n",
    "\n",
    "# Establish the timestamp for starting this model run (used for filenames)\n",
    "import datetime\n",
    "stamp = datetime.datetime.now().strftime(\"%Y-%m-%d_%H%M%S\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "2a803d9d",
   "metadata": {},
   "source": [
    "#### Enter initial input information and view input data\n",
    "\n",
    "Edit and run the cells below to enter the name of your input data file and then load and display the input table and figures."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "f7153fbc",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Name the run (manually edit, then run the cell)\n",
    "runname = 'sample'"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "a720a373",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Set up file directories:\n",
    "maindir = 'outputs/{}'.format(runname)\n",
    "outputs = 'outputs'\n",
    "if not os.path.exists(maindir):\n",
    "    os.mkdir(maindir)\n",
    "\n",
    "stampdir = 'outputs/{}/{}'.format(runname, stamp)\n",
    "if os.path.exists(stampdir):\n",
    "    shutil.rmtree(stampdir)\n",
    "else:\n",
    "    os.mkdir(stampdir)\n",
    "os.mkdir('outputs/{}/{}/Figures'.format(runname,stamp))\n",
    "os.mkdir('outputs/{}/{}/Results'.format(runname,stamp))\n",
    "os.mkdir('outputs/{}/{}/Additional_calculations'.format(runname,stamp))\n",
    "\n",
    "runlog = open(\"outputs/{}/{}/run_log_{}.txt\".format(runname,stamp,stamp),\"w\")\n",
    "runlog.write('Title: Run log for dynamic melting using file {}.csv; run initiated at timestamp {}\\n'.format(runname,stamp))\n",
    "runlog.close()\n",
    "\n",
    "runstamp = 'outputs/{}/{}/'.format(runname,stamp)\n",
    "figures = 'outputs/{}/{}/Figures/'.format(runname,stamp)\n",
    "results = 'outputs/{}/{}/Results/'.format(runname,stamp)\n",
    "runlog_file = \"outputs/{}/{}/run_log_{}.txt\".format(runname,stamp,stamp)\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "c46204b4",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Input pandas dataframe\n",
    "input_file = 'data/{}.csv'.format(runname)\n",
    "\n",
    "comment='Comment'\n",
    "with open(input_file) as f_obj:\n",
    "    reader = csv.reader(f_obj, delimiter=',')\n",
    "    for line in reader:\n",
    "        if comment in str(line):\n",
    "            df = pd.read_csv(input_file,skiprows=1,dtype=float)\n",
    "        else:\n",
    "            df = pd.read_csv(input_file,dtype=float)\n",
    "        break\n",
    "\n",
    "runlog = open(runlog_file,\"a\")\n",
    "runlog.write('\\n')\n",
    "runlog.write('Melting Model: DYNAMIC ANALYTICAL SOLUTION\\n')\n",
    "runlog.write('Input data file:')\n",
    "runlog.close()\n",
    "\n",
    "df.to_csv(runlog_file, index=None, sep=' ', mode='a')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "23a056dd",
   "metadata": {},
   "outputs": [],
   "source": [
    "df"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "b17d3a94",
   "metadata": {},
   "outputs": [],
   "source": [
    "UserCalc.plot_inputs(df);"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e90b7bc0",
   "metadata": {},
   "source": [
    "#### Modifying inputs for lithospheric transport\n",
    "For scenarios where a truncated melting scheme is desired to simulate a lithospheric cap, the cells below will truncate the 1D melting regime at a final melting pressure, $P_{Lithos}$, defined by the user. Run the cells below to define a $P_{Lithos}$ value; set this value to 0.0 if no lithospheric cap is desired.\n",
    "\n",
    "There are then two options for using this value, or not. The first option deletes all rows from the input table for depths shallower than $P_{Lithos}$, completely truncating the melting calculation. (This creates a new input table without overwriting the initial version.) The second option leaves the input table as-is, allowing melting to continue to the top of the column as initially defined. This is useful for, say, testing model results for the same input lithology, but testing a range of different capping depths."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "538be15a",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Run this cell to define the lithospheric boundary pressure.\n",
    "# Plithos = input(\"What is the pressure at the base of the lithosphere where melting stops (in kilobars)? For example, for a layer that truncates melting at 5 kbar, answer '5.0'.\")\n",
    "\n",
    "# Alternative approach: edit and uncomment the line below, comment the command above, and then run the cell:\n",
    "Plithos = 5.0"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "be2a4c1a",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Run this cell to implement the choice made above.\n",
    "Plithos = float(Plithos)\n",
    "df_nolith = df[df.P > Plithos]\n",
    "df_lith = df.copy()\n",
    "Pfinal = df_lith.iloc[(df_lith['P']-Plithos).abs().idxmin()]\n",
    "F_max = Pfinal[1].tolist()\n",
    "df_lith.loc[(df_lith['P'] < Plithos),['F']] = F_max\n",
    "\n",
    "lithlist = ['',\n",
    "            '',\n",
    "            'Lithospheric pressure (Pfinal): {} kbar'.format(Plithos),\n",
    "            '']\n",
    "with open(runlog_file,\"a\") as runlog:\n",
    "    for i in lithlist:\n",
    "        runlog.write(i+'\\n')\n",
    "runlog.close()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ca8095df",
   "metadata": {},
   "source": [
    "The cells below offer options for modifying the model inputs by truncating the column at the base of the lithosphere, or not."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "a4c03562",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Specify whether to truncate the input file at the lithosphere for this run, or not:.\n",
    "# lith_option=input(\"Should melting STOP at the base of the lithosphere? Options are 'yes' or 'no'.\")\n",
    "\n",
    "# Alternative approach: edit and uncomment the line below, comment the command above, and then run the cell:\n",
    "lith_option = 'yes'"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "91d31e83",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Run this cell to implement the selected data input option for the rest of the model run\n",
    "if lith_option == 'no':\n",
    "    df_input = df\n",
    "    lith_text = \"melting was calculated as in the input file without modification from\"\n",
    "    \n",
    "elif lith_option == 'yes':\n",
    "    df_input = df_nolith\n",
    "    lith_text = \"transport and melting were truncated at\"\n",
    "    \n",
    "inputnew = ['',\n",
    "            'Lithospheric transport: Magma {} {} kbar.'.format(lith_text,Plithos),\n",
    "            '',\n",
    "            'Updated input run conditions:']\n",
    "with open(runlog_file,\"a\") as runlog:\n",
    "    for i in inputnew:\n",
    "        runlog.write(i+'\\n')\n",
    "runlog.close()\n",
    "\n",
    "df_input.to_csv(runlog_file, index=None, sep=' ', mode='a')\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "6f1b06ae",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Generate a figure for the input scenario, and save it to the output folder:\n",
    "UserCalc.plot_inputs(df_input)\n",
    "plt.savefig(figures+\"{}_dynamic_inputs.pdf\".format(runname),transparent=True,dpi=300,bbox_inches='tight')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c276bd54",
   "metadata": {},
   "source": [
    "#### Additional run parameters\n",
    "\n",
    "Before running the model, check and edit the series of cells below to change the default physical input parameters to those desired for a single 1D model run. This is useful both to test the model is working, and to generate outputs that save results along the entire melting path.\n",
    "\n",
    "Note that the initial U concentration and Th/U ratio are only needed to calculate equiline ($(^{230}Th/^{232}Th)$ vs. $(^{238}U/^{232}Th)$) diagrams for 1D column runs; other initial concentrations are most easily entered as initial activity ratios. If preferred, the initial $(^{238}U/^{232}Th)$ ratio can be entered as an activity ratio intead.\n",
    "\n",
    "The default initial parent-daughter activity ratios are all in secular equilibrium (= 1.0), but other values can be entered below if preferred.\n",
    "\n",
    "Edit and run the cells to save the parameter values."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "85c791c9",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Input parameters for 1D model run\n",
    "\n",
    "# Maximum melt porosity:\n",
    "phi0 = 0.008\n",
    "\n",
    "# Solid upwelling rate in cm/yr.:\n",
    "W = 3.\n",
    "\n",
    "# Decay constants in 1/a\n",
    "lam231=0.00002116\n",
    "lam226=0.0004332\n",
    "lam230=0.000009158\n",
    "lam238=1.55125e-10\n",
    "lam235=9.8486e-10\n",
    "lam232=4.948e-11"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "f022ed31",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Retrieve initial D values\n",
    "DU=df['DU']\n",
    "DTh=df['DTh']\n",
    "DPa=df['DPa']\n",
    "DRa=df['DRa']\n",
    "DU0 = DU[0]\n",
    "DTh0 = DTh[0]\n",
    "DRa0 = DRa[0]\n",
    "DPa0 = DPa[0]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "49941d86",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Set initial activity ratios and U activities:\n",
    "Th_U_init = 1.0\n",
    "Ra_Th_init = 1.0\n",
    "Pa_U_init = 1.0\n",
    "alpha0_238U = 1.0\n",
    "alpha0_235U = 1.0"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "bce0d4e4",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Define additional variables and scaling factors:\n",
    "rho_s = 3300.   # kg/m3\n",
    "rho_f = 2800.   # kg/m3\n",
    "dPdz = 0.32373  # Depth conversion factor from kbar to km\n",
    "W0 = W/1.e5  # Convert to km/yr\n",
    "P = df_input['P']\n",
    "dep = P/dPdz"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "f6f9ae33",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Define time interval of interest:\n",
    "P = df_input['P']\n",
    "steps = len(P)\n",
    "t_final = max(dep)/W0 - min(dep)/W0\n",
    "t_eval = np.linspace(0., t_final, steps).tolist()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "9b28785e",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Determine a linear slope to the melting rate\n",
    "F = df_input['F']\n",
    "dFdt,intercept = np.polyfit(t_eval, F, 1)\n",
    "Mfit = dFdt*rho_s"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "bb43e2f8",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Determine other activities and initial concentrations:\n",
    "\n",
    "# Equiline option 1: Determine the initial (232Th) from U conc and a Th/U ratio:\n",
    "# U_init = 0.05    # (ppm)\n",
    "# Th_U_ratio = 3.\n",
    "# alpha0_232Th = alpha0_238U*Th_U_ratio*(4.948e-11*1./232.03805)/(1.551e-10*0.9927995/238.02891)\n",
    "\n",
    "# Equiline option 2: Directly define the initial (238U/232Th) ratio:\n",
    "U238_Th232_init = 1.0\n",
    "alpha0_232Th = alpha0_238U / U238_Th232_init\n",
    "\n",
    "# Determine other initial activities:\n",
    "alpha0_230Th = alpha0_238U * Th_U_init\n",
    "alpha0_226Ra = alpha0_230Th * Ra_Th_init\n",
    "alpha0_231Pa = alpha0_235U * Pa_U_init\n",
    "\n",
    "# Initial activities (assuming secular equilibrium):\n",
    "U238_0 = 1./(DU0*rho_s-DU0*phi0*rho_s+phi0*rho_f)\n",
    "Th230_0 = 1./(DTh0*rho_s-DTh0*phi0*rho_s+phi0*rho_f)\n",
    "U235_0 = 1./(DU0*rho_s-DU0*phi0*rho_s+phi0*rho_f)\n",
    "Pa231_0 = 1./(DPa0*rho_s-DPa0*phi0*rho_s+phi0*rho_f)\n",
    "Ra226_0 = 1./(DRa0*rho_s-DRa0*phi0*rho_s+phi0*rho_f)\n",
    "\n",
    "# Save key model parameters to the runlog\n",
    "params = ['',\n",
    "          'Initial 1D run parameters:',\n",
    "          'phi0 = {}'.format(phi0),\n",
    "          'W0 = {} cm/yr.'.format(W0),\n",
    "          'rho_s = {} kg/m3'.format(rho_s),\n",
    "          'rho_f = {} kg/m3'.format(rho_f),\n",
    "          'Final time of melting = {} yrs.'.format(t_final),\n",
    "          'Initial (230Th/238U) = {}'.format(Th_U_init),\n",
    "          'Initial (226Ra/230Th) = {}'.format(Ra_Th_init),\n",
    "          'Initial (231Pa/235U) = {}'.format(Pa_U_init),\n",
    "          'Initial (238U/232Th) = {}'.format(U238_Th232_init)\n",
    "         ]\n",
    "with open(runlog_file,\"a\") as runlog:\n",
    "    for i in params:\n",
    "        runlog.write(i+'\\n')\n",
    "runlog.close()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "fae00bd0",
   "metadata": {},
   "source": [
    "#### Determine analytical solutions\n",
    "\n",
    "The next few cells calculate partial melting compositions:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "8993f190",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Calculate dynamic melts using Zou and Zindler's analytical solution:\n",
    "FU = (phi0*rho_f)/((DU0*rho_s)*(1-phi0)+(phi0*rho_f))\n",
    "FTh = (phi0*rho_f)/((DTh0*rho_s)*(1-phi0)+(phi0*rho_f))\n",
    "FPa = (phi0*rho_f)/((DPa0*rho_s)*(1-phi0)+(phi0*rho_f))\n",
    "FRa = (phi0*rho_f)/((DRa0*rho_s)*(1-phi0)+(phi0*rho_f))\n",
    "alphaU = ((FU*(1-DU0))/(phi0*rho_f))*Mfit\n",
    "alphaTh = ((FTh*(1-DTh0))/(phi0*rho_f))*Mfit\n",
    "alphaPa = ((FPa*(1-DPa0))/(phi0*rho_f))*Mfit\n",
    "alphaRa = ((FRa*(1-DRa0))/(phi0*rho_f))*Mfit"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "f5422106",
   "metadata": {},
   "outputs": [],
   "source": [
    "# \"Residual\" melts from Zou and Zindler (2000):\n",
    "U238_f = np.zeros_like(t_eval)\n",
    "Th230_f = np.zeros_like(t_eval)\n",
    "Ra226_f = np.zeros_like(t_eval)\n",
    "U235_f = np.zeros_like(t_eval)\n",
    "Pa231_f = np.zeros_like(t_eval)\n",
    "\n",
    "for n,t in enumerate(t_eval):\n",
    "    U238_f[n] = U238_0*math.exp(-(alphaU+lam238)*t)\n",
    "    Th230_f[n] = Th230_0*math.exp(-(alphaTh+lam230)*t)+lam230/(alphaTh+lam230-alphaU-lam238)*(FTh/FU)*U238_0*(math.exp(-(alphaU+lam238)*t)-math.exp(-(alphaTh+lam230)*t))\n",
    "    Ra226_f[n] = Ra226_0*math.exp(-(alphaRa+lam226)*t)+lam226/(alphaRa+lam226-alphaTh-lam226)*(FRa/FTh)*Th230_0*(math.exp(-(alphaTh+lam230)*t)-math.exp(-(alphaRa+lam226)*t))+lam230*lam226/(alphaTh+lam230-alphaU-lam238)*(FRa/FU)*U238_0*((math.exp(-(alphaU+lam238)*t)-math.exp(-(alphaRa+lam226)*t))/(alphaRa+lam226-alphaU-lam238)-(math.exp(-(alphaTh+lam230)*t)-math.exp(-(alphaRa+lam226)*t))/(alphaRa+lam226-alphaTh-lam230))\n",
    "    U235_f[n] = U235_0*math.exp(-(alphaU+lam235)*t)\n",
    "    Pa231_f[n] = Pa231_0*math.exp(-(alphaPa+lam231)*t)+lam231/(alphaPa+lam231-alphaU-lam235)*(FPa/FU)*U235_0*(math.exp(-(alphaU+lam235)*t)-math.exp(-(alphaPa+lam231)*t))\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "5d2efbc5",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Display activity ratios in \"residual\" (instantaneous) melts:\n",
    "Th230_f/U238_f"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "0bdea7f7",
   "metadata": {},
   "outputs": [],
   "source": [
    "Ra226_f/Th230_f"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "270c3904",
   "metadata": {},
   "outputs": [],
   "source": [
    "Pa231_f/U235_f"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "a6690b0c",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Zou and Zindler convenience functions:\n",
    "theta = Mfit/(rho_s*(1-phi0))\n",
    "c1 = lam230/(alphaTh+lam230-alphaU-lam238)\n",
    "c2 = (alphaU+lam238+theta)*(alphaTh-alphaU-lam238)/(alphaTh+lam230+theta)/(alphaTh+lam230-alphaU-lam238)\n",
    "c3 = lam230*lam226/(alphaTh+lam230-alphaU-lam238)/(alphaRa+lam226-alphaU-lam238)\n",
    "c4 = lam226*(alphaU+lam238+theta)/(alphaTh+lam230+theta)/(alphaRa+lam226-alphaTh-lam230) * (alphaTh-alphaU-lam238)/(alphaTh+lam230-alphaU-lam238)\n",
    "c5 = (alphaU+lam238+theta)/(alphaRa+lam226+theta)/(alphaRa+lam226-alphaTh-lam230) * (alphaRa-alphaTh-lam230+(lam230*lam226)/(alphaRa+lam226-alphaU-lam238))\n",
    "c6 = lam231/(alphaPa+lam231-alphaU-lam235)\n",
    "c7 = (alphaU+lam235+theta)*(alphaPa-alphaU-lam235)/(alphaPa+lam231+theta)/(alphaPa+lam231-alphaU-lam235)\n",
    "\n",
    "# Extracted melts:\n",
    "actThU = np.zeros_like(t_eval)\n",
    "actRaTh = np.zeros_like(t_eval)\n",
    "actPaU = np.zeros_like(t_eval)\n",
    "\n",
    "for n,t in enumerate(t_eval):\n",
    "    actThU[n] = FTh/FU*(c1+c2*(1-math.exp(-(alphaTh+lam230+theta)*t))/(1-math.exp(-(alphaU+lam238+theta)*t)))\n",
    "    actRaTh[n] = FRa/FTh*(c3*(1-math.exp(-(alphaU+lam238+theta)*t))+c4*(1-math.exp(-(alphaTh+lam230+theta)*t))+c5*(1-math.exp(-(alphaRa+lam226+theta)*t)))/(c1*(1-math.exp(-(alphaU+lam238+theta)*t))+c2*(1-math.exp(-(alphaTh+lam230+theta)*t)))\n",
    "    actPaU[n] = FPa/FU*(c6+c7*(1-math.exp(-(alphaPa+lam231+theta)*t))/(1-math.exp(-(alphaU+lam235+theta)*t)))\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "4cfedb09",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Display activity ratios in \"extracted\" (integrated or accumulated) melts:\n",
    "actThU"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "f7830a5a",
   "metadata": {},
   "outputs": [],
   "source": [
    "actRaTh"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "1990468e",
   "metadata": {},
   "outputs": [],
   "source": [
    "actPaU"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "248f0fb9",
   "metadata": {},
   "source": [
    "#### Save final partial melt compositions\n",
    "\n",
    "The output table below saves the resulting activity ratios."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "1f5c6322",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Build the output dataframe\n",
    "df_out = pd.DataFrame()\n",
    "df_out['t'] = t_eval\n",
    "df_out['P'] = df_input['P']\n",
    "df_out['depth'] = P/dPdz\n",
    "df_out['F'] = df_input['F']\n",
    "df_out['phi'] = phi0\n",
    "\n",
    "df_out['(230Th/238U)'] = actThU\n",
    "df_out['(226Ra/230Th)'] = actRaTh\n",
    "df_out['(231Pa/235U)'] = actPaU\n",
    "# df_out['(230Th/232Th)'] = Th230_Th232_c\n",
    "# df_out['(238U/232Th)'] = U238_Th232_c\n",
    "\n",
    "df_out['Instantaneous (230Th/238U)'] = Th230_f/U238_f\n",
    "df_out['Instantaneous (226Ra/230Th)'] = Ra226_f/Th230_f\n",
    "df_out['Instantaneous (231Pa/235U)'] = Pa231_f/U235_f\n",
    "# df_out['Instantaneous (230Th/232Th)'] = Th230_Th232_f\n",
    "# df_out['Instantaneous (238U/232Th)'] = U238_Th232_f\n",
    "\n",
    "runlog = open(runlog_file,\"a\")\n",
    "runlog.write('\\n')\n",
    "runlog.write('Dynamic melting results: \\n')\n",
    "runlog.close()\n",
    "df_out.to_csv(runlog_file, index=None, sep=' ', mode='a')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "eafa068b",
   "metadata": {},
   "outputs": [],
   "source": [
    "# View the summary results table\n",
    "df_out"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "50792af1",
   "metadata": {},
   "outputs": [],
   "source": [
    "# 1D model results figure\n",
    "fig = UserCalc.plot_1Dcolumn(df_out)\n",
    "plt.savefig(figures+\"{}_dynamic__1D_depths.pdf\".format(runname),transparent=True,dpi=300,bbox_inches='tight')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "e4e75fe6",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Export results dataframe with activity ratios:\n",
    "with open(results+\"{}_dynamic_1D_solution.csv\".format(runname), 'a') as comment:\n",
    "    comment.write(\"Comment:, Dynamic melting model results where: porosity={}; solid upwelling={} cm/yr.; initial (230Th/238U)={}; (226Ra/230Th)={}; (231Pa/235U)={}; and {} at {} kbar. \\n\".format(phi0,W,Th_U_init,Ra_Th_init,Pa_U_init,lith_text,Plithos))\n",
    "    df_out.to_csv(comment, index=False)\n",
    "    "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9e74ff00",
   "metadata": {},
   "source": [
    "### Batch operations\n",
    "\n",
    "If batch operations are being calculated, edit and run the cells below to calculate outcomes over a range of maximum residual melt porosity ($\\phi$) and the solid mantle upwelling rate ($W$) values. Edit the first cell to select whether to define the specific $\\phi$ and $W$ values as evenly spaced log grid intervals or using manually specified values (default). All upwelling rates are entered in units of cm/yr. Other input parameters will match those set above."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "19b04b9d",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Edit the rows below to specify reference porosity and solid upwelling rates for batch runs.\n",
    "\n",
    "# phi0s = np.logspace(-3,-2,11)\n",
    "phi0s = np.array([0.001, 0.002, 0.005, 0.01, 0.02])\n",
    "\n",
    "# Ws = np.logspace(-1,1,11)\n",
    "Ws = np.array([0.5, 1., 2., 5., 10., 20., 50.])  # in cm/yr.\n",
    "\n",
    "W0s = Ws/1.e5  # convert units to km/yr. for calculations below"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "ee5d63d5",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Build labels for output tables using phi0 and W0 values:\n",
    "phi0list = phi0s.tolist()\n",
    "phi0str = []\n",
    "for s in phi0list:\n",
    "    phi0str.append(str(s))\n",
    "label = 'phi0='\n",
    "cols = [label + y for y in phi0str]\n",
    "\n",
    "W0list = Ws.tolist()\n",
    "W0str = []\n",
    "for x in W0list:\n",
    "    W0str.append(str(x))\n",
    "label2 = 'W0='\n",
    "units = ' cm/yr.'\n",
    "rows = [label2 + y + units for y in W0str]\n",
    "\n",
    "runlog = open(runlog_file,\"a\")\n",
    "runlog.write('\\n')\n",
    "runlog.write('Setting up batch operations.\\n')\n",
    "runlog.close()\n",
    "\n",
    "with open(runlog_file,\"a\") as runlog:\n",
    "    runlog.write('Run parameters for batch operations: \\n')\n",
    "    for i in cols:\n",
    "        runlog.write(i+'\\n')\n",
    "    for j in rows:\n",
    "        runlog.write(j+'\\n')\n",
    "runlog.close()\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "b62f637e",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Enable time counter to visually track calculation progress:\n",
    "import time\n",
    "tic = time.perf_counter()\n",
    "toc = time.perf_counter()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "caadddcf",
   "metadata": {},
   "source": [
    "The next cells run the model iteratively over the range of input values you have defined. Note that this can sometimes take several minutes to complete."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "b6ce407a",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Create blank arrays to populate with results for final melt compositions:\n",
    "actThRa = np.zeros((2,len(W0s),len(phi0s)))\n",
    "actPa = np.zeros((1,len(W0s),len(phi0s)))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "79880ad8",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Run the calculation loop and store results:\n",
    "for n, W0 in enumerate(W0s):\n",
    "    t_final = max(dep)/W0\n",
    "    t_eval = np.linspace(0., t_final, steps).tolist()\n",
    "    dFdt,intercept = np.polyfit(t_eval, F, 1)\n",
    "    M = dFdt*rho_s\n",
    "    \n",
    "    for m, phi0 in enumerate(phi0s):\n",
    "        # Calculate convenience functions:\n",
    "        FU = (phi0*rho_f)/((DU0*rho_s)*(1-phi0)+(phi0*rho_f))\n",
    "        FTh = (phi0*rho_f)/((DTh0*rho_s)*(1-phi0)+(phi0*rho_f))\n",
    "        FPa = (phi0*rho_f)/((DPa0*rho_s)*(1-phi0)+(phi0*rho_f))\n",
    "        FRa = (phi0*rho_f)/((DRa0*rho_s)*(1-phi0)+(phi0*rho_f))\n",
    "        alphaU = ((FU*(1-DU0))/(phi0*rho_f))*M\n",
    "        alphaTh = ((FTh*(1-DTh0))/(phi0*rho_f))*M\n",
    "        alphaPa = ((FPa*(1-DPa0))/(phi0*rho_f))*M\n",
    "        alphaRa = ((FRa*(1-DRa0))/(phi0*rho_f))*M\n",
    "        \n",
    "        theta = M/(rho_s*(1-phi0))\n",
    "        c1 = lam230/(alphaTh+lam230-alphaU-lam238)\n",
    "        c2 = (alphaU+lam238+theta)*(alphaTh-alphaU-lam238)/(alphaTh+lam230+theta)/(alphaTh+lam230-alphaU-lam238)\n",
    "        c3 = lam230*lam226/(alphaTh+lam230-alphaU-lam238)/(alphaRa+lam226-alphaU-lam238)\n",
    "        c4 = lam226*(alphaU+lam238+theta)/(alphaTh+lam230+theta)/(alphaRa+lam226-alphaTh-lam230) * (alphaTh-alphaU-lam238)/(alphaTh+lam230-alphaU-lam238)\n",
    "        c5 = (alphaU+lam238+theta)/(alphaRa+lam226+theta)/(alphaRa+lam226-alphaTh-lam230) * (alphaRa-alphaTh-lam230+(lam230*lam226)/(alphaRa+lam226-alphaU-lam238))\n",
    "        c6 = lam231/(alphaPa+lam231-alphaU-lam235)\n",
    "        c7 = (alphaU+lam235+theta)*(alphaPa-alphaU-lam235)/(alphaPa+lam231+theta)/(alphaPa+lam231-alphaU-lam235)\n",
    "        \n",
    "        # Activity ratios in extracted melts:\n",
    "        actThRa[0,n,m] = FTh/FU*(c1 + c2*(1-math.exp(-(alphaTh+lam230+theta)*t_final))/(1-math.exp(-(alphaU+lam238+theta)*t_final)))\n",
    "        actThRa[1,n,m] = FRa/FTh*(c3*(1-math.exp(-(alphaU+lam238+theta)*t_final))+c4*(1-math.exp(-(alphaTh+lam230+theta)*t_final))+c5*(1-math.exp(-(alphaRa+lam226+theta)*t_final)))/(c1*(1-math.exp(-(alphaU+lam238+theta)*t_final))+c2*(1-math.exp(-(alphaTh+lam230+theta)*t_final)))\n",
    "        actPa[0,n,m] = FPa/FU*(c6 + c6*(1-math.exp(-(alphaPa+lam231+theta)*t_final))/(1-math.exp(-(alphaU+lam235+theta)*t_final)))\n",
    "\n",
    "Thgrid = actThRa[0]\n",
    "Thgrid = np.where(Thgrid == 0, np.nan, Thgrid)\n",
    "Ragrid = actThRa[1]\n",
    "Ragrid = np.where(Ragrid == 0, np.nan, Ragrid)\n",
    "Pagrid = actPa[0]\n",
    "Pagrid = np.where(Pagrid == 0, np.nan, Pagrid)\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "012e2d74",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Format results data tables and save outputs:\n",
    "\n",
    "dfTh = pd.DataFrame(Thgrid)\n",
    "dfTh.columns = cols\n",
    "dfTh.index = rows\n",
    "runlog = open(runlog_file,\"a\")\n",
    "runlog.write('\\n')\n",
    "runlog.write('Batch (230Th/238U) results for simple dynamic melting: \\n')\n",
    "runlog.close()\n",
    "dfTh.to_csv(runlog_file, sep=' ', mode='a')\n",
    "with open(results+\"{}_dynamic_batch_Th.csv\".format(runname), 'a') as comment_batchTh:\n",
    "    comment_batchTh.write(\"Comment:, 1D analytical solution to dynamic melting batch results for (230Th/238U) using initial (230Th/238U)={}; (226Ra/230Th)={}; (231Pa/235U)={}; and where {} at {} kbar. \\n\".format(Th_U_init,Ra_Th_init,Pa_U_init,lith_text,Plithos))\n",
    "    dfTh.to_csv(comment_batchTh)\n",
    "\n",
    "dfRa = pd.DataFrame(Ragrid)\n",
    "dfRa.columns = cols\n",
    "dfRa.index = rows\n",
    "runlog = open(runlog_file,\"a\")\n",
    "runlog.write('\\n')\n",
    "runlog.write('Batch (226Th/230Th) results for simple dynamic melting: \\n')\n",
    "runlog.close()\n",
    "dfRa.to_csv(runlog_file, sep=' ', mode='a')\n",
    "with open(results+\"{}_dynamic_batch_Ra.csv\".format(runname), 'a') as comment_batchRa:\n",
    "    comment_batchRa.write(\"Comment:, 1D analytical solution to dynamic melting batch results for (226Ra/230Th) using initial (230Th/238U)={}; (226Ra/230Th)={}; (231Pa/235U)={}; and where {} at {} kbar. \\n\".format(Th_U_init,Ra_Th_init,Pa_U_init,lith_text,Plithos))\n",
    "    dfRa.to_csv(comment_batchRa)\n",
    "\n",
    "dfPa = pd.DataFrame(Pagrid)\n",
    "dfPa.columns = cols\n",
    "dfPa.index = rows\n",
    "runlog = open(runlog_file,\"a\")\n",
    "runlog.write('\\n')\n",
    "runlog.write('Batch (231Pa/235U) results for simple dynamic melting: \\n')\n",
    "runlog.close()\n",
    "dfPa.to_csv(runlog_file, sep=' ', mode='a')\n",
    "with open(results+\"{}_dynamic_batch_Pa.csv\".format(runname), 'a') as comment_batchPa:\n",
    "    comment_batchPa.write(\"Comment:, 1D analytical solution to dynamic melting batch results for (231Pa/235U) using initial (230Th/238U)={}; (226Ra/230Th)={}; (231Pa/235U)={}; and where {} at {} kbar. \\n\".format(Th_U_init,Ra_Th_init,Pa_U_init,lith_text,Plithos))\n",
    "    dfPa.to_csv(comment_batchPa)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "219798c5",
   "metadata": {},
   "source": [
    "#### View batch results\n",
    "\n",
    "The cells below generate contour figures and gridded mesh diagrams for the batch run outputs above."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "33352445",
   "metadata": {},
   "outputs": [],
   "source": [
    "UserCalc.plot_contours(phi0s,Ws,actThRa,figsize=(12,12))\n",
    "plt.suptitle('Dynamic melting',y=0.99,fontsize=30);\n",
    "plt.savefig(figures+\"{}_dynamic_contour_Th_Ra.pdf\".format(runname),transparent=True,dpi=300,bbox_inches='tight')\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "d9d32766",
   "metadata": {},
   "outputs": [],
   "source": [
    "UserCalc.plot_contours(phi0s,Ws,actPa)\n",
    "plt.suptitle('Dynamic melting',fontsize=22);\n",
    "plt.savefig(figures+\"{}_dynamic_contour_Pa.pdf\".format(runname),transparent=True,dpi=300,bbox_inches='tight')\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "85494fa8",
   "metadata": {},
   "outputs": [],
   "source": [
    "UserCalc.plot_mesh_Ra(Thgrid,Ragrid,Ws,phi0s)\n",
    "plt.title('Dynamic melting',y=1.1);\n",
    "plt.savefig(figures+\"{}_dynamic_grid_Ra_Th.pdf\".format(runname),transparent=True,dpi=300,bbox_inches='tight')\n",
    "plt.savefig(figures+\"{}_dynamic_grid_Ra_Th.ps\".format(runname),dpi=300,bbox_inches='tight')\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "e305b220",
   "metadata": {},
   "outputs": [],
   "source": [
    "UserCalc.plot_mesh_Pa(Thgrid,Pagrid,Ws,phi0s)\n",
    "plt.title('Dynamic melting',y=1.1);\n",
    "plt.savefig(figures+\"{}_dynamic_grid_Pa_Th.pdf\".format(runname),transparent=True,dpi=300,bbox_inches='tight')\n",
    "plt.savefig(figures+\"{}_dynamic_grid_Pa_Th.ps\".format(runname),transparent=True,dpi=300,bbox_inches='tight')\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "2cf660f6",
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.12"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
