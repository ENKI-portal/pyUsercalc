{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Input File Smoothing Calculator\n",
    "#### Lynne J. Elkins$^{1}$\n",
    "\n",
    "$^{1}$ University of Nebraska-Lincoln, Lincoln, NE, USA, lelkins@unl.edu"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Summary\n",
    "\n",
    "This notebook aims to address an occasional problem when using the pyUserCalc and stableCalc code for melting model calculations, where an ODE solver fails to find a solution to the melting equations because of sharp discontinuities or change points in the input parameters with depth. The tool below either permits the user to identify the locations of such change points manually, or attempts to locate them using automatic change point finding routines. The notebook then applies a smoothing function for each sharp, discontinuous change, and generates a modified input file that will hopefully be better resolved by the interpolation functions and ODE solvers in the pyUserCalc calculator tool. While this does not always work and sometimes minor manual adjustment of the input data file is still necessary, this notebook provides a fast way to try to fix the problem before attempting a manual edit. Note that this notebook generates a new input file with a different name; it does not overwrite the existing file.\n",
    "\n",
    "To use the tool below, the user should already have identified the problematic input file, and it should be saved in the user's 'data' file directory. Several options are provided for identifying indexed locations that may require smoothing; these are explained in more detail below."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Retrieving and analyzing the problematic input file"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The cell below imports the needed code libraries. Run this cell."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import pandas as pd\n",
    "import numpy as np\n",
    "import scipy\n",
    "from scipy import signal\n",
    "from scipy.signal import savgol_filter, find_peaks_cwt\n",
    "import csv\n",
    "from collections import OrderedDict\n",
    "import matplotlib.pyplot as plt\n",
    "%matplotlib inline\n",
    "import UserCalc\n",
    "import stableCalc"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Next, load and view the input file that needs to be assessed for abrupt changes. In the first cell below, change the run name from 'sample' to the appropriate CSV file name (minus the file extension) to load the desired input file. The second cell loads the problematic input file as a dataframe, which will be edited using the tools below and then saved as a new (renamed) input file."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Identify the problematic input file:\n",
    "runname='sample'"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Load the problematic input file identified above:\n",
    "input_file = 'data/{}.csv'.format(runname)\n",
    "\n",
    "# Check for comment line in file, and if present, ignore it to build the dataframe:\n",
    "comment='Comment'\n",
    "with open(input_file) as f_obj:\n",
    "    reader = csv.reader(f_obj, delimiter=',')\n",
    "    for line in reader:\n",
    "        if comment in str(line):\n",
    "            df_initial = pd.read_csv(input_file,skiprows=1,dtype=float)\n",
    "        else:\n",
    "            df_initial = pd.read_csv(input_file,dtype=float)\n",
    "        break\n",
    "\n",
    "df_initial"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Manual point selection\n",
    "The next cell displays the input variables for $F$ and $D_i$ with depth, for simple visual inspection."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Plot unmodified input file for initial observation and confirmation:\n",
    "stableCalc.plot_inputs(df_initial);"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "It is often possible to observe and estimate the pressure(s) where any abrupt changes occur simply by inspecting the data table and input figure shown above. One straightforward approach is thus simply to manually identify and list the pressures where the input file needs to be smoothed.\n",
    "\n",
    "In the first cell below, you may choose to enter the pressures where the data should be smoothed for this input file. Then run the following two cells to save and confirm those values. (Note that the default listed values shown are simply examples, and any number of values can be entered as a list.)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Manually enter the pressure(s) (in kbar) that should be smoothed in the list below.\n",
    "# List syntax: Plist = [ value, value, value, ...]\n",
    "\n",
    "Plist = [ 10., 20. ]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# This cell will store the index locations for the manually entered values above\n",
    "Pressure = np.array(df_initial['P'])\n",
    "length = len(Plist)\n",
    "manual = []\n",
    "\n",
    "for j in range(length):\n",
    "    value = Plist[j]\n",
    "    index = np.where(Pressure == value)\n",
    "    number = int(index[0])\n",
    "    manual.append(number)\n",
    "    \n",
    "manual.sort()\n",
    "print(manual)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Automated change point routines\n",
    "Alternatively, it might be unclear where the pyUserCalc ODE solver was finding a discontinuity that it was unable to solve, or there may be several such potential locations. It can sometimes be useful to use one or more methods for automatically locating abrupt changes in the data set by examining the second derivatives of the input variables. The cells below attempt two different methods for identifying possible change points. These techniques sometimes identify additional curves or inflection points that are not in fact discontinuous, so they probably do not need to be smoothed, but one or both methods can also often locate abrupt changes that might be causing problems. We recommend running *all* the cells below in order to see what the automatic change point routines can locate. You do not need to then *use* all of these results: this is simply gathering data for later evaluation."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# First, export an array to be evaluated for discontinuities.\n",
    "headers = df_initial.columns.tolist()\n",
    "initialarray = df_initial.values"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "initialarray"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now calculate second derivatives for each column, and look for where they are large:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Calculate and identify second derivatives for each variable:\n",
    "window_percentage = 0.1  # window size as a percentage of data length\n",
    "peak_widths = np.arange(1, 1000)\n",
    "\n",
    "num_columns = initialarray.shape[1]\n",
    "change_points = []\n",
    "\n",
    "num_rows = initialarray.shape[0]\n",
    "window = max(3, int(window_percentage * num_rows))  # Ensure window_length is at least 3\n",
    "window += 1 if window % 2 == 0 else 0  # Ensure window_length is odd\n",
    "\n",
    "for i in range(num_columns):\n",
    "    col_data = initialarray[:, i]\n",
    "    col_der2 = savgol_filter(initialarray[:,i], window_length=window, polyorder=2, deriv=2)\n",
    "    col_max_der2 = np.max(np.abs(col_der2))\n",
    "    col_large = np.where(np.abs(col_der2) > col_max_der2/2)[0]\n",
    "    col_gaps = np.diff(col_large) > window\n",
    "    col_begins = np.insert(col_large[1:][col_gaps], 0, col_large[0])\n",
    "    col_ends = np.append(col_large[:-1][col_gaps], col_large[-1])\n",
    "    col_changes = ((col_begins+col_ends)/2).astype(int)\n",
    "    change_points.append(col_changes)\n",
    "change_points"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The second method below tries a peak finding signal routine to locate second derivative peaks:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Set up a second loop to run the peak finding routine for each array column i and store results:\n",
    "# for i in... :\n",
    "peaks = []\n",
    "for i in range(num_columns):\n",
    "    col_peakind = signal.find_peaks_cwt(col_der2, np.arange(1,1000))\n",
    "    peaks.append(col_peakind)\n",
    "col_peakind"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Finally, plot both sets of results for each variable. For each plot, the outcomes of calculating large second derivatives is shown in red, and the results of the peak finding signal routine are shown in blue:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "fig, axs = plt.subplots(num_columns, 1, figsize=(10, 5 * num_columns), squeeze=False)\n",
    "\n",
    "for i in range(num_columns):\n",
    "    col_data = initialarray[:, i]\n",
    "    col_changes = change_points[i]\n",
    "    col_peaks = peaks[i]\n",
    "    col_name = headers[i]\n",
    "\n",
    "    axs[i, 0].plot(col_data, label='Data')\n",
    "\n",
    "    if len(col_changes) > 0:\n",
    "        axs[i, 0].plot(col_changes, col_data[col_changes], 'ro', label='Change Points')\n",
    "    else:\n",
    "        axs[i, 0].text(0.5, 0.5, 'No change points found', horizontalalignment='center', verticalalignment='center', transform=axs[i, 0].transAxes)\n",
    "\n",
    "    if len(col_peaks) > 0:\n",
    "        axs[i, 0].plot(col_peaks, col_data[col_peaks], 'bo', label='Peaks')\n",
    "    else:\n",
    "        axs[i, 0].text(0.5, 0.5, 'No peaks found', horizontalalignment='center', verticalalignment='center', transform=axs[i, 0].transAxes)\n",
    "\n",
    "    axs[i, 0].set_title(f'Discontinuities and Peaks in {col_name}')\n",
    "    axs[i, 0].legend()\n",
    "\n",
    "plt.tight_layout()\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Analysis of results\n",
    "Because the change point location routines used above may have identified some locations that are not actually problematic or discontinuous, two further options are presented below to build the list of locations that will actually be modified.\n",
    "\n",
    "The first option will automatically compile *all* of the identified locations from all of the finding routines above, and will apply a smoothing routine to *everything.* In many (but not all) cases, this second approach is overkill, but it is also relatively harmless, since the smoothed intervals are generally relatively small and the changes to the input data tend to be minor. This may not be a good choice for all scenarios, though.\n",
    "\n",
    "In the second option, the user can simply evaluate all of the findings to manually select and enter a subset of index values that should be fixed, as a list.\n",
    "\n",
    "Both options can be set up and saved using the next two cells, though not all will actually be used, and they will not overwrite each other. For now, edit and run both of these cells just to store your observations."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Option 1: Generate automated list of change points and peaks based on second derivatives.\n",
    "\n",
    "# Initialize an empty list to store all change points and peaks\n",
    "all_points = []\n",
    "\n",
    "# Iterate over the change points and peaks lists\n",
    "for col_changes, col_peaks in zip(change_points, peaks):\n",
    "    # Extend the all_points list with the change points and peaks of each column\n",
    "    all_points.extend(col_changes)\n",
    "    all_points.extend(col_peaks)\n",
    "\n",
    "# Remove duplicates from the list\n",
    "all_points = list(set(all_points))\n",
    "\n",
    "# Sort the list in numerical order\n",
    "all_points.sort()\n",
    "\n",
    "# Print the concatenated list of all change points and peaks\n",
    "print(\"List of all automatic points:\", all_points)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Option 2: Manually enter selected index values based on plots and list above:\n",
    "selected = [ 475, 578 ]\n",
    "selected"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Altogether, the methods above provided three options for identifying abrupt discontinuities in the input data file: initial manual identification of those points, simply using the full automated list, or manual selection of a subset of the automatically generated list.\n",
    "\n",
    "In the cell below, identify which method is preferred. This will then produce a list of locations to be smoothed."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Name the method to be used for identifying change point locations in this smoothing operation.\n",
    "# To select the initial, manual list, enter 'manual' below; for values that were picked from the\n",
    "# automatically generated list, enter 'selected'; and for the full automated list, enter 'auto'.\n",
    "method = 'auto'"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Run this cell to store the choice made above.\n",
    "if method == 'manual':\n",
    "    change_list = manual\n",
    "\n",
    "elif method == 'auto':\n",
    "    change_list = all_points\n",
    "    \n",
    "elif method == 'selected':\n",
    "    change_list = selected\n",
    "\n",
    "change_list"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For the option chosen above, the smoothing function below will replace a series of sequential rows from the input file with modified rows where the variables change more gradually, hopefully making it easier for the melting model to calculate interpolated values and find a numerical solution.\n",
    "\n",
    "However, if any of the index values in the desired list are very close together, only a single smoothing function is needed for that cluster, making them redundant. The next cell simply finds and consolidates any particularly close or clustered values in your list, treating them as duplicates."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Omit any indices that are too close together for the smoothing routine to distinguish\n",
    "# (that is, because smoothing occurs over a pressure interval, the same operation will cover \n",
    "# several indices if they are very close):\n",
    "\n",
    "usedValues = set()\n",
    "changes = []\n",
    "\n",
    "for v in change_list:\n",
    "    if v not in usedValues:\n",
    "        changes.append(v)\n",
    "\n",
    "        for lv in range(v - 6, v + 7):\n",
    "            usedValues.add(lv)\n",
    "\n",
    "print(changes)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Apply smoothing functions and save a modified pyUserCalc input file"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The following cells use the problematic locations identified above to build a revised, transitional data frame that smooths the input values for $F$, $K_r$, and $D_i$ over an interval surrounding each identified row.\n",
    "\n",
    "The default setting below applies the smoothing function for an interval of 15 steps (that is, smoothing between the values 7 steps above and 7 steps below the change point). This can be edited by changing the \"N\" value to a different integer value."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Define the total interval (number of rows/steps) over which the data should be smoothed\n",
    "# on either side of each change point:\n",
    "N = 20\n",
    "P_lambda = 1.\n",
    "HalfN = (N-1)/2"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "# This cell defines a dataframe to be worked on below, without overwriting the original.\n",
    "# It also sets up additional indexing to track modified rows during the smoothing operation.\n",
    "\n",
    "df_smooth = df_initial.copy()\n",
    "df_smooth.insert(0, 'New_ID', range(0, 0 + len(df_smooth)))\n",
    "df_smooth"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Create a temporary dataframe with just D values as columns, and extract the header names:\n",
    "Di = [col for col in df_initial if col.startswith('D')]\n",
    "dfDs = df_initial[Di]\n",
    "D_labels = dfDs.columns.values.tolist()\n",
    "\n",
    "# Iterate over all change points\n",
    "for index in changes:\n",
    "    lower = int(index - HalfN)\n",
    "    upper = int(index + HalfN)\n",
    "    P_range = [df_initial.at[lower,'P'],df_initial.at[index,'P'],df_initial.at[upper,'P']]\n",
    "    F_range = [df_initial.at[lower,'F'], df_initial.at[index,'F'], df_initial.at[upper,'F']] \n",
    "    \n",
    "    if 'Kr' in df_initial.columns:\n",
    "        Kr_range = [df_initial.at[lower,'Kr'], df_initial.at[upper,'Kr']]\n",
    "    else:\n",
    "        Kr_range = 'None'\n",
    "    \n",
    "    D_lower = [df_initial.at[lower, '{}'.format(i)] for i in D_labels]\n",
    "    D_upper = [df_initial.at[upper, '{}'.format(i)] for i in D_labels]\n",
    "    \n",
    "    New_ID = [df_smooth.at[lower,'New_ID'],df_smooth.at[upper,'New_ID']]\n",
    "    \n",
    "    df_snip = stableCalc.smoothing(New_ID, P_range, F_range, Kr_range, D_lower, D_upper, D_labels, N, P_lambda)\n",
    "    df_snip_indexed = df_snip.set_index(['New_ID'])\n",
    "    df_smooth.update(df_snip_indexed, overwrite=True)\n",
    "\n",
    "# Remove the 'New_ID' column from the smoothed DataFrame\n",
    "df_smooth.drop(columns=['New_ID'], inplace=True)\n",
    "df_smooth"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The cell below displays the outcomes of the smoothing functions:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# View the results in a pyUserCalc input plot:\n",
    "fig = stableCalc.plot_inputs(df_smooth)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If the outcome of the smoothing routine looks satisfactory, run the final cell to export a modified and renamed data input file for future melting model calculations. If not, it is possible to attempt to smooth over a different list of points or for different intervals by modifying the cells above, then clearing and restarting the kernel and rerunning the notebook.\n",
    "\n",
    "Finally, these functions work best for moderate slopes and changes in direction. For particularly sharp turns (like those approaching 90º on the input plots), it may be necessary (and will probably be easier and faster) to just attempt some manual tuning. That is, it is recommended to smooth out the sharp corners in the input files by simply adjusting values for several rows above and below the abrupt changes, and seeing how they look."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Save the results as a revised and renamed csv file in the data folder:\n",
    "with open('data/{}_smoothed.csv'.format(runname),'a') as text:\n",
    "    text.write(\"Comment:,Smoothed input data table modified from '{}' input. \\n\".format(runname))\n",
    "    df_smooth.to_csv(text)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.12"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
