{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Simple Spreadsheet Calculator\n",
    "\n",
    "This notebook demonstrates the use of the `twolayermodel` convenience function, which returns an input dataframe for pyUserCalc using a smoothed two layer model similar to that provided as a spreadsheet calculator in the original UserCalc program (Spiegelman, 2000).\n",
    "\n",
    "Additional documentation for this calculator is available using the cell below. Remove the preferred comment (#) tag and run the cells below to view the documentation."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import pandas as pd\n",
    "import numpy as np\n",
    "from collections import OrderedDict\n",
    "import matplotlib.pyplot as plt\n",
    "%matplotlib inline\n",
    "\n",
    "# Import UserCalc:\n",
    "import UserCalc\n",
    "import stableCalc"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#UserCalc.twolayermodel?\n",
    "#stableCalc.inputmodel?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "There are two separate tools below, for U-series inputs and for stable element concentrations. The default setup serves as an example for generating input files from a two-layer template. The default example constructs a dataframe with $N=41$ rows, using a two-layer model where the lower layer extends from 40-20 kbar and the upper layer from 20-0 kbar. The properties of each layer transition smoothly over a lengthscale of approximately $P_\\lambda = 1$ kbar.\n",
    "\n",
    "To use either tool below to generate user-defined input files from the template, first modify the named values and variables in the cell below:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Define the input data for two layer model:\n",
    "\n",
    "# Number of rows in the dataframe (integer value)\n",
    "N = 41\n",
    "\n",
    "# Pressure [Bottom, layer_boundary, Top] in kbar. Note that the bottom and top\n",
    "# pressures must be decimal values, and the layer boundary must be an integer.\n",
    "P = [40., 20, 0.]\n",
    "layer_boundary = P[1]\n",
    "\n",
    "# Pressure smoothing parameter (in kbar, must be decimal value)\n",
    "P_lambda = 1."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The next cell specifies that the degree of melting in the lower layer will range linearly from $F=0$ to $F=0.05$, and in the upper layer from $F=0.05$ to $F=0.2$. Note that only simple linear changes in $F$ with depth are possible in this template, but each layer can have a different $dF/dP$ slope."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Decimal degree of melting [bottom, layer boundary, top]\n",
    "F = [0., 0.05, 0.2]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### U-series Input Calculator Tool\n",
    "\n",
    "For U-series calculations and input files, modify the U-series specific values below. If only stable elements will be used, skip to the stable calculator tool below."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Give the input file a unique name:\n",
    "twolayerinput = 'test_twolayer_model'"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Each layer has its own relative permeability and constant bulk partition coefficients for $D_U$, $D_{Th}$, $D_{Ra}$ and $D_{Pa}$. These must all be decimal values:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Relative permeability of each layer\n",
    "Kr_lower = 1.\n",
    "Kr_upper = 1.\n",
    "Kr = [ Kr_lower, Kr_upper ]\n",
    "\n",
    "# Bulk partition coefficients for lower and upper layers [DU, DTh, DRa, DPa]\n",
    "D_lower = [ 0.009, 0.005, 0.0002, 0.0001 ]\n",
    "D_upper = [ 0.005, 0.004, 0.0002, 0.0001 ]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "##### Usage:\n",
    "\n",
    "The cells below call the two-layer model using the input values specified above, then display the results as a dataframe and a depth figure for visual inspection. Normally these cells should be run without editing."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "df = UserCalc.twolayermodel(P,F,Kr,D_lower,D_upper,N,P_lambda)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "df.head()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig = UserCalc.plot_inputs(df);"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Save the results\n",
    "The cell below saves the template result as a new input table in the data folder. This can be used as an input file to run the pyUserCalc melting model. Note that the comment text embedded in the input table can be modified below if desired."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "with open('data/{}.csv'.format(twolayerinput), 'a') as comment:\n",
    "    comment.write(\"Comment:, Two-layer input from template with layer boundary at P={} kbar. \\n\".format(layer_boundary))\n",
    "    df.set_index(['P']).to_csv(comment)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Stable Element Input Calculator Tool\n",
    "\n",
    "The setup below operates similarly to the U-series calculator above, except for a user-specified number of elements. To use the tool below to generate user-defined input files from the template, modify the named values and variables as needed. Note that if using both tools to create paired U-series and stable inputs, they must have different file names to avoid overwriting."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Give the input file a unique name:\n",
    "stableinput = 'test_stable_model'"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In the cell below, modify the default list to include the elements to be analyzed. They must be in quotations."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "elements = ['Rb', 'Sr', 'Sm', 'Nd', 'Lu', 'Hf', 'U', 'Th', 'Pb']"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Next, assign lower and upper layer partition coefficients for the same elements, for the order of elements used above:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Bulk partition coefficients for lower and upper layers\n",
    "D_lower = [ 0.0001, 0.0002, 0.0003, 0.0004, 0.0005, 0.0006, 0.0007, 0.0008, 0.0009 ]\n",
    "D_upper = [ 0.001, 0.002, 0.003, 0.004, 0.005, 0.006, 0.007, 0.008, 0.009 ]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Run the cell below to check that the outputs look correct:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "for a, b in zip(elements, D_lower):\n",
    "    print(\"D({}) = {} in the lower layer\".format(a, b))\n",
    "\n",
    "for a, b in zip(elements, D_upper):\n",
    "    print(\"D({}) = {} in the upper layer\".format(a, b))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "##### Usage:\n",
    "\n",
    "As above, the cells below call the stable element two-layer model using the input values specified above, then display the results as a dataframe and a depth figure for visual inspection. Normally these cells should be run without editing."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "df = stableCalc.inputmodel(P,F,elements,D_lower,D_upper,N,P_lambda)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "df.head()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig = stableCalc.plot_inputs(df);"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Save the results\n",
    "The cell below saves the template result as a new input table in the data folder. This can be used as an input file to run the stableCalc melting model. Note that the comment text embedded in the input table can be modified below if desired."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "with open('data/{}.csv'.format(stableinput), 'a') as comment:\n",
    "    comment.write(\"Comment:, Two-layer input from template with layer boundary at P={} kbar. \\n\".format(layer_boundary))\n",
    "    df.set_index(['P']).to_csv(comment)\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.12"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
