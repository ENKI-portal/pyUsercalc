{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "99acd908",
   "metadata": {},
   "source": [
    "# Input File Generator from Mineral Modes\n",
    "#### Lynne J. Elkins$^{1}$\n",
    "\n",
    "$^{1}$ University of Nebraska-Lincoln, Lincoln, NE, USA, lelkins@unl.edu"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "377f0191",
   "metadata": {},
   "source": [
    "### Summary\n",
    "\n",
    "This notebook is a simple tool that generates UserCalc input files given degrees of melting and mineral modes with depth, and fixed mineral/melt partition coefficients. This is intended to be useful when converting the results of petrologic modeling that produce mineral modes with depth into UserCalc calculation files for trace element concentration and U-series disequilibria during partial melting. The default is to generate a combined U-series and stable element tool if both calculations are desired, but it is also possible to set up separate input files for each by running this generator twice, each time for different elements. The reference permeability column is inserted as a default column with values of 1.0, which can be edited separately in the .CSV file produced, if desired.\n",
    "\n",
    "To use this tool, two .CSV input tables will be necessary, one for mineral modes with depth and the other for partition coefficients for each mineral phase and element of interest. An example template for each will be displayed in the code below.\n",
    "\n",
    "First, run the first cell below to load necessary libraries. If you have not used a Jupyter notebook before, select the cell with a mouse-click and then type Shift + Return to run the code in that cell."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "d819aabb",
   "metadata": {},
   "outputs": [],
   "source": [
    "import pandas as pd\n",
    "import numpy as np\n",
    "import csv\n",
    "import matplotlib.pyplot as plt\n",
    "%matplotlib inline\n",
    "import UserCalc\n",
    "import stableCalc"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "05441674",
   "metadata": {},
   "source": [
    "#### Load initial data table\n",
    "The next cell will generate a simple example table showing how to format a comma-delimited text (.csv) file to set up the desired inputs. This table should contain columns for pressure (P) using constant pressure steps, for the degree of melting (F), and for the mode of each mineral."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "b0be8742",
   "metadata": {},
   "outputs": [],
   "source": [
    "# View example data table:\n",
    "Ptemplate = {'P':[10.,9.,8.,7.,6.,5.],\n",
    "           'F':[0.,0.01,0.02,0.03,0.04,0.05],\n",
    "           'ol':[0.6,0.6,0.6,0.6,0.6,0.6],\n",
    "           'opx':[0.2,0.2,0.2,0.2,0.2,0.2],\n",
    "           'cpx':[0.2,0.2,0.2,0.2,0.2,0.2],\n",
    "            'gt':[0.,0.,0.,0.,0.,0.]}\n",
    "df_template = pd.DataFrame(Ptemplate)\n",
    "df_template"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f1222af6",
   "metadata": {},
   "source": [
    "Once an input .CSV file has been created with these columns and appropriate data values for the full range of pressures to be considered, place the file in the 'data' folder for this code directory and provide the file name in the cell below to load and view the data.\n",
    "\n",
    "Note that there can be as few or as many minerals as desired, and the minerals can have any names; the names will simply be retrieved from the column headers. The only important restriction is that the same names or abbreviations be used in the accompanying partition coefficient file. Pressure should be in kilobars, and all other values should be decimal percents. Note that the displayed version of the table may be abbreviated below, but this is just a visual check to confirm that you loaded the correct file and it is in the right format. The accompanying figure will then display the mineral modes with depth."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "8aca0981",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Identify the file name:\n",
    "modefile='modesPer1300.csv'"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "ebee3906",
   "metadata": {
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "# Load and view the mineral mode file identified above:\n",
    "modetable = 'data/{}'.format(modefile)\n",
    "with open(modetable) as f_obj:\n",
    "    reader = csv.reader(f_obj, delimiter=',')\n",
    "    modes = pd.read_csv(modetable,dtype=float)\n",
    "modescopy = modes.copy()\n",
    "modescopy.drop(['P','F'], axis=1, inplace=True)\n",
    "modes"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "4e6296f4",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Display mineral modes with depth:\n",
    "\n",
    "minerals = [col for col in modes.columns if col not in [\"P\", \"F\"]]\n",
    "fig, (ax1, ax2) = plt.subplots(1,2, sharey=True)\n",
    "ax1.plot(modes['F']*100,modes['P'])\n",
    "ax1.invert_yaxis()\n",
    "ax1.set_xlabel('F (%)')\n",
    "ax1.set_ylabel('Pressure (kbar)')\n",
    "xticks = np.linspace(0,max(modes['F']),10)\n",
    "ax1.set_xticks(xticks,minor=True)\n",
    "ax1.grid()\n",
    "for s in minerals:\n",
    "    ax2.plot(modes[s]*100,modes['P'],label=s)\n",
    "ax2.set_xlabel(r'Mode (%)')\n",
    "ax2.grid()\n",
    "ax2.legend(loc='best',bbox_to_anchor=(1.1,1));\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3c699727",
   "metadata": {},
   "source": [
    "#### Calculating bulk partition coefficients\n",
    "\n",
    "To determine bulk partition coefficients, it is necessary to define constant mineral/melt partition coefficients for each element.\n",
    "\n",
    "The next cell shows the list of minerals from the input table that was already loaded above. This is simply another visual check to make sure the mineral names or abbreviations are consistent across both data tables."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "ead3c810",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Display list of mineral names from the input table above:\n",
    "minerals"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "39c3c19c",
   "metadata": {},
   "source": [
    "The cells below will display an example data table for mineral/melt partition coefficients; your .CSV input file(s) for partition coefficients should be arranged in the same format with similar headers. Note that the mineral names need to match the table loaded above, and the element names will be extracted from the .CSV file loaded below.\n",
    "\n",
    "Note that it is possible to load two different partition coefficient data files if you wish to assign different mineral/melt partition coefficients based on depth for a two-layer peridotitic source. This is useful for scenarios where the partition coefficients change with pressure, so a batch of partition coefficients can be selected depending on whether or not garnet is present. If this is not necessary, the easiest approach is to just load the same file twice. Note that if this condition is desired, the code below will look for non-zero modes in the column with the header **'gt'**. The code does this to determine the presence or absence of garnet, and will then calculate the corresponding partition coefficients.\n",
    "\n",
    "It is also possible to instruct the code to run a partition coefficient calculation based on pressure, instead of entering a constant value for $D_i$. To do this, write the arithmetic formula into the cell in the .CSV file using mathematical notation; this will be loaded as a text string and then parsed as a formula when calculating partition coefficients below. The mathematical formula can also include an `if...else` statement for pressure dependency. The example data table includes text strings that will display a formula in the table for $D_{Th}$ and $D_{U}$ (these example formulas are from Krein et al. (2021) after Landwehr et al. (2001)'s results). When used, this formula would set $D_{Th}$ equal to $0.30 - 0.0005 * P$ for pressures up to 30 kbar, and instead would use a constant scalar value for all pressures greater than 30 kbar. The part beginning with `if...` can be omitted if this pressure dependency is not needed. (Note that the .CSV file itself does *not* require quotations to be present in the formula-enabled entry, even though the example script within the cell below does need this to display correctly here. The .CSV file should simply look like the table that prints below if it is viewed as a spreadsheet, such as in MS Excel.)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "ca71f2cb",
   "metadata": {},
   "outputs": [],
   "source": [
    "# View example data table:\n",
    "indices = ['ol','opx','gt','cpx']\n",
    "Dtemplate = {'U':[0.0005,0.001,0.01,'0.204+0.01956*P if P <= 30.0 else 0.03'],\n",
    "             'Th':[0.001,0.01,0.02,'0.30-0.0005*P if P <= 30.0 else 0.1'],\n",
    "             'Pa':[0.6,0.6,0.,0.6,],\n",
    "             'Ra':[0.2,0.2,0.,0.2,],\n",
    "             'Rb':[0.2,0.2,0.2,0.2],\n",
    "             'Sr':[0.7,0.1,0.1,0.2]}\n",
    "df_template = pd.DataFrame(Dtemplate,index=indices)\n",
    "df_template"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "318ba280",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Provide the name of the partition coefficient data tables, formatted with minerals \n",
    "# as column labels and elements as row labels in the leftmost column, as in the template above:\n",
    "\n",
    "# Identify the file names:\n",
    "Dfile1= 'DSpPer.csv'  # Deep/garnet-bearing lithology\n",
    "Dfile2 = 'DGtPer.csv'  # Shallow/garnet-free lithology"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "7e72009b",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Load and view the first mineral mode file:\n",
    "Dtable1 = 'data/{}'.format(Dfile1)\n",
    "with open(Dtable1) as f_obj:\n",
    "    reader = csv.reader(f_obj, delimiter=',')\n",
    "    Ddf1 = pd.read_csv(Dtable1,index_col=0)\n",
    "Ddf1"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "d22fc273",
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "# Load and view the second file, if it is different:\n",
    "if Dfile1 != Dfile2:\n",
    "    Dtable2 = 'data/{}'.format(Dfile2)\n",
    "    with open(Dtable2) as f_obj:\n",
    "        reader = csv.reader(f_obj, delimiter=',')\n",
    "        Ddf2 = pd.read_csv(Dtable2,index_col=0)\n",
    "else:\n",
    "    print('Note: Only one set of partition coefficients has been loaded.')\n",
    "    Ddf2 = Ddf1\n",
    "Ddf2"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a91582ad",
   "metadata": {},
   "source": [
    "The next cell verifies that the partition coefficient inputs have the correct minerals required for the modes that were loaded above, and lists the elements provided as a verification that the list is complete. If there is a mismatch, edit and reload the partition coefficient tables in the cells above, then try again."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "46d4b732",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Check that the partition coefficient input tables have the correct minerals and elements:\n",
    "elements1 = [col for col in Ddf1.columns if col not in [\"P\", \"F\"]]\n",
    "elements2 = [col for col in Ddf2.columns if col not in [\"P\", \"F\"]]\n",
    "elements = elements1\n",
    "print('Check that the elements provided in both partition coefficient files are correct:')\n",
    "print('List of elements in Dfile1: {}'.format(elements1))\n",
    "print('List of elements in Dfile2: {}'.format(elements2))\n",
    "\n",
    "missing_minerals1 = set(minerals) - set(Ddf1.index)\n",
    "\n",
    "missing_minerals2 = set(minerals) - set(Ddf2.index)\n",
    "\n",
    "if missing_minerals1:\n",
    "    raise ValueError(f\"Dfile1 is missing the following minerals: {missing_minerals1}\")\n",
    "else:\n",
    "    print('Dfile1 mineral list looks correct.')\n",
    "\n",
    "if missing_minerals2:\n",
    "    raise ValueError(f\"Dfile2 is missing the following minerals: {missing_minerals2}\")\n",
    "else:\n",
    "    print('Dfile2 mineral list looks correct.')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "050e9fcd",
   "metadata": {},
   "source": [
    "The next cells determine the partition coefficients with pressure, and saves them to a dataframe. First, the minerals and elements are put in order so they match for matrix algebra, and then a dataframe is set up. Note that the table displayed below will be empty of values for now; it will be filled later."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "02a406b7",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Reorder the rows of Ddf1 and Ddf2 to match the minerals list\n",
    "Ddf1 = Ddf1.loc[minerals]\n",
    "Ddf2 = Ddf2.loc[minerals]\n",
    "\n",
    "# Reorder the columns of Ddf1 and Ddf2 to match the elements list\n",
    "Ddf1 = Ddf1[elements]\n",
    "Ddf2 = Ddf2[elements]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "d01e99c9",
   "metadata": {
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "# Assemble empty dataframe 'df', with column headers and values for P and F copied from above.\n",
    "df = pd.DataFrame()\n",
    "df['P'] = modes['P'].copy()\n",
    "df['F'] = modes['F'].copy()\n",
    "Ds = [\"D\" + element for element in elements]\n",
    "for D in Ds:\n",
    "    df[D] = np.nan\n",
    "df"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a1544ce5",
   "metadata": {},
   "source": [
    "The next cell calculates the bulk partition coefficients and stores them for each row:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "9955b5ff",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Check if 'gt' column exists in modescopy dataframe\n",
    "gt_column_exists = 'gt' in modescopy.columns\n",
    "\n",
    "def evaluate_formula(formula, P):\n",
    "    try:\n",
    "        return eval(formula)\n",
    "    except Exception as e:\n",
    "        raise ValueError(f\"Error evaluating formula '{formula}' with P={P}: {e}\")\n",
    "\n",
    "# Calculate bulk partition coefficients:\n",
    "for elem in elements:\n",
    "    for i in range(len(df)):\n",
    "        mode = modescopy.iloc[i].to_numpy()\n",
    "        P_value = modes.loc[i, 'P']\n",
    "        if gt_column_exists and modescopy.loc[i, 'gt'] > 0.00001:\n",
    "            D_values = Ddf1[elem]\n",
    "        else:\n",
    "            D_values = Ddf2[elem]\n",
    "        D_values = D_values.apply(lambda x: evaluate_formula(str(x), P_value) if isinstance(x, str) else x)\n",
    "        Dbulk = np.dot(mode, D_values.to_numpy())\n",
    "        df.loc[i, \"D\" + elem] = Dbulk\n",
    "df"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5894611a",
   "metadata": {},
   "source": [
    "#### Generate the UserCalc input file\n",
    "The remaining cells create and save the input data file for UserCalc.\n",
    "\n",
    "The first cell inserts a $K_r$ column. Note that this column will only contain default values of 1.0 for the reference permeabilities used in `UserCalc`, but it can be edited outside of this notebook by editing the .CSV directly if preferred. Stable element calculations will simply ignore this column since they do not depend on permeabilities or elemental residence times."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "512636f4",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Create default Kr column with values of 1.0:\n",
    "df.insert(2, 'Kr', np.full_like(df['P'], 1.0))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a41b8bd5",
   "metadata": {},
   "source": [
    "Finally, the cells below produce tables and figures showing the data inputs determined above, to make sure they look correct before saving."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "c8ce2a5e",
   "metadata": {},
   "outputs": [],
   "source": [
    "# View the final data table:\n",
    "df"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "4aacaf03",
   "metadata": {},
   "outputs": [],
   "source": [
    "# View the results using UserCalc and StableCalc plots:\n",
    "required_columns = ['DU', 'DTh', 'DPa', 'DRa']\n",
    "if all(col in df.columns for col in required_columns):\n",
    "    UserCalc.plot_inputs(df)\n",
    "    plt.title(\"U-series partition coefficients\")\n",
    "\n",
    "stableCalc.plot_inputs(df)\n",
    "plt.title(\"All partition coefficients\");"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "28868158",
   "metadata": {},
   "source": [
    "If the results above look correct, in the next cells, give your input file a unique name, and then write a brief string of text within the quotations describing what this input file contains. This text should not contain any commas, because it will be used to create the comment line when generating the final results file, which is *comma-delimited*; semi-colons are better. Aim for useful caption text that will make it easy to distinguish between your input files and clarify what they are used for."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "4d8278e2",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Give your input file a unique name inside the quotes below:\n",
    "inputname = 'inputtest'"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "23efe958",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Add a brief, descriptive caption to store with the input file for easy identification:\n",
    "comment = 'Peridotite melting along an adiabat with potential temperature of 1300 C; input generator test.'"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c1d314de",
   "metadata": {},
   "source": [
    "The final cell below exports the input file as a commented .csv, and saves it to the data folder:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "9e200c42",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Export the output dataframe:\n",
    "with open ('data/{}.csv'.format(inputname),'a') as commentline:\n",
    "    commentline.write('Comment:, '+comment+'\\n')\n",
    "    df.to_csv(commentline,index=False)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "da4171b8",
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.12"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
